package com.richestsoft.gift3r.views.dialogfragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

/**
 * Created by Mukesh on 14/04/2016.
 */
public class DatePickerDialogFragment extends DialogFragment {

    public static final String BUNDLE_EXTRAS_YEAR = "year";
    public static final String BUNDLE_EXTRAS_MONTH = "month";
    public static final String BUNDLE_EXTRAS_DAY = "day";
    public static final String BUNDLE_EXTRAS_MIN_DATE = "minDate";
    public static final String BUNDLE_EXTRAS_MAX_DATE = "maxDate";

    DatePickerDialog.OnDateSetListener onDateSetListener;

    public static DatePickerDialogFragment newInstance(int year, int month, int day,
                                                       Long minDate, Long maxDate) {
        DatePickerDialogFragment datePickerDialogFragment = new DatePickerDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_EXTRAS_YEAR, year);
        bundle.putInt(BUNDLE_EXTRAS_MONTH, month);
        bundle.putInt(BUNDLE_EXTRAS_DAY, day);
        bundle.putLong(BUNDLE_EXTRAS_MIN_DATE, minDate);
        bundle.putLong(BUNDLE_EXTRAS_MAX_DATE, maxDate);
        datePickerDialogFragment.setArguments(bundle);
        return datePickerDialogFragment;
    }

    public void setCallBack(DatePickerDialog.OnDateSetListener onDateSetListener) {
        this.onDateSetListener = onDateSetListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DatePickerDialog datePickerDialog;
        if (null != getArguments()) {
            datePickerDialog = new DatePickerDialog(getActivity(),
                    onDateSetListener, getArguments().getInt(BUNDLE_EXTRAS_YEAR),
                    getArguments().getInt(BUNDLE_EXTRAS_MONTH), getArguments()
                    .getInt(BUNDLE_EXTRAS_DAY));

            // get and set min date
            Long date = getArguments().getLong(BUNDLE_EXTRAS_MIN_DATE, 0);
            datePickerDialog.getDatePicker().setMinDate(date);

            // get and set max date
            date = getArguments().getLong(BUNDLE_EXTRAS_MAX_DATE, 0);
            datePickerDialog.getDatePicker().setMaxDate(date);
        } else {
            datePickerDialog = new DatePickerDialog(getActivity(),
                    onDateSetListener, 1, 1, 1971);
        }
        return datePickerDialog;
    }
}
