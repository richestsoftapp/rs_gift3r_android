package com.richestsoft.gift3r.views.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.pojos.Country;
import com.richestsoft.gift3r.views.dialogfragments.CountryCodesDialogFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mukesh on 08/12/2016.
 */

public class CountriesListAdapter extends
        RecyclerView.Adapter<CountriesListAdapter.CountryCodeViewHolder> {

    public static final String INTENT_EXTRAS_COUNTRY_CODE = "countryCode";

    private CountryCodesDialogFragment mCounCountryCodesDialogFragment;
    private List<Country> countriesList;
    private List<Country> originalCountryCodesList = new ArrayList<>();
    private LayoutInflater mLayoutInflater;

    public CountriesListAdapter(CountryCodesDialogFragment countryCodesDialogFragment,
                                List<Country> countriesList) {
        mCounCountryCodesDialogFragment = countryCodesDialogFragment;
        this.countriesList = countriesList;
        originalCountryCodesList.addAll(countriesList);
        mLayoutInflater = LayoutInflater.from(mCounCountryCodesDialogFragment.getActivity());
    }

    @Override
    public CountryCodeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CountryCodeViewHolder(mLayoutInflater.inflate(R.layout.row_country,
                parent, false));
    }

    @Override
    public void onBindViewHolder(CountryCodeViewHolder countryCodeViewHolder, int position) {
        Country country = countriesList.get(position);
        countryCodeViewHolder.tvCountryName.setText(country.getName());
        countryCodeViewHolder.tvCountryCode.setText(country.getDial_code());
    }

    @Override
    public int getItemCount() {
        return countriesList.size();
    }

    public void searchCountry(String searchedKeyword) {
        countriesList.clear();
        if (searchedKeyword.isEmpty()) {
            countriesList.addAll(originalCountryCodesList);
        } else {
            for (Country country : originalCountryCodesList) {
                if (country.getName().toLowerCase().contains(searchedKeyword)) {
                    countriesList.add(country);
                }
            }
        }
        notifyDataSetChanged();
    }

    class CountryCodeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.llParent) LinearLayout llParent;
        @BindView(R.id.tvCountryName) AppCompatTextView tvCountryName;
        @BindView(R.id.tvCountryCode) AppCompatTextView tvCountryCode;

        CountryCodeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            llParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCounCountryCodesDialogFragment.dismiss();
                    if (null != mCounCountryCodesDialogFragment.getTargetFragment()) {
                        mCounCountryCodesDialogFragment.getTargetFragment()
                                .onActivityResult(mCounCountryCodesDialogFragment
                                                .getTargetRequestCode(), Activity.RESULT_OK,
                                        new Intent().putExtra(INTENT_EXTRAS_COUNTRY_CODE,
                                                countriesList.get(getAdapterPosition())
                                                        .getDial_code()));
                    }
                }
            });
        }
    }
}
