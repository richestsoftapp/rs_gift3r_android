package com.richestsoft.gift3r.views.adapters;

import android.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.pojos.Store;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.utils.ScreenDimensions;
import com.richestsoft.gift3r.views.fragments.StoreDetailFragment;
import com.richestsoft.gift3r.views.fragments.StoreListFragment;
import com.richestsoft.gift3r.views.interfaces.LoadMoreListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by user28 on 8/2/18.
 */

public class StoreListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int ROW_TYPE_STORE = 0;
    public final int ROW_TYPE_LOADER = 1;

    private LayoutInflater mLayoutInflater;
    private List<Store> storeList;
    private Fragment mFragment;
    private int storeImageSize;
    private LoadMoreListener mLoadMoreListener;
    private boolean hasMoreStores;

    public StoreListAdapter(Fragment fragment) {
        mFragment = fragment;
        storeList = new ArrayList<>();
        mLayoutInflater = LayoutInflater.from(fragment.getActivity());
        mLoadMoreListener = (LoadMoreListener) fragment;
        storeImageSize = (int) ((new ScreenDimensions(fragment.getActivity()).getScreenWidth() - 3 * fragment.getActivity().getResources()
                        .getDimension(R.dimen.store_row_offset)) / 2);
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (ROW_TYPE_LOADER == viewType) {
            return new LoadMoreViewHolder(mLayoutInflater.inflate
                    (R.layout.row_load_more, parent, false));
        }
        return new StoreViewHolder(mLayoutInflater.inflate(
                R.layout.row_store, parent, false));
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // Loader type row
        if (ROW_TYPE_LOADER == getItemViewType(position)) {
            LoadMoreViewHolder loadMoreViewHolder = (LoadMoreViewHolder) holder;
            if (null != mLoadMoreListener && hasMoreStores) {
                loadMoreViewHolder.tvNoMoreData.setVisibility(View.GONE);
                loadMoreViewHolder.progressBar.setVisibility(View.VISIBLE);
                mLoadMoreListener.onLoadMore();
            } else {
                loadMoreViewHolder.progressBar.setVisibility(View.GONE);
                loadMoreViewHolder.tvNoMoreData.setVisibility(View.VISIBLE);
                loadMoreViewHolder.tvNoMoreData
                        .setText(mFragment.getActivity().getString(R.string.no_more_stores));
            }
        }
        // Store type row
        else {
            StoreViewHolder storeViewHolder = (StoreViewHolder) holder;
            Store store = storeList.get(position);

            storeViewHolder.tvStoreName.setText(store.getName());

            //storeViewHolder.sdvStoreImage.setImageURI(GeneralFunctions.getResizedImageUri(store.getImage(), storeImageSize, storeImageSize));
            storeViewHolder.sdvStoreImage.setImageURI(store.getImage());
        }
    }

    @Override public int getItemCount() {
        return StoreListFragment.STORES_PAGE_LIMIT <= storeList.size() ? storeList.size() + 1 : storeList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (StoreListFragment.STORES_PAGE_LIMIT <= storeList.size() && position == storeList.size()) {
            return ROW_TYPE_LOADER;
        } else {
            return ROW_TYPE_STORE;
        }
    }

    public void updateList(ArrayList<Store> storeList, int page) {
        // add data to main list
        if (1 == page) {
            this.storeList.clear();
        }
        this.storeList.addAll(storeList);
        notifyDataSetChanged();

        // flag for more stores
        hasMoreStores = StoreListFragment.STORES_PAGE_LIMIT <= storeList.size();
    }

    class StoreViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cardViewStore) CardView cardViewStore;

        @BindView(R.id.sdvStoreImage) SimpleDraweeView sdvStoreImage;

        @BindView(R.id.tvStoreName) TextView tvStoreName;

        StoreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            // set cardView width
            cardViewStore.getLayoutParams().width = storeImageSize;
        }

        @OnClick({R.id.cardViewStore})
        public void onCLicked(View view) {
            switch (view.getId()) {
                case R.id.cardViewStore:
                    GeneralFunctions.addFragment(mFragment.getFragmentManager(), R.animator.slide_right_in, 0, 0, R.animator.slide_right_out, StoreDetailFragment.newInstance(storeList.get(getAdapterPosition()),"1"), null, R.id.flFragmentContainerHome);
                    break;
            }
        }
    }

    class LoadMoreViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvNoMoreData) TextView tvNoMoreData;
        @BindView(R.id.progressBar) ProgressBar progressBar;

        LoadMoreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
