package com.richestsoft.gift3r.views.fragments;

import android.net.Uri;
import android.os.Bundle;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.dialogfragments.ImagePreviewDialogFragment;
import com.richestsoft.gift3r.views.utils.frescozoomabledraweeview.zoomable.ZoomableDraweeView;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;

import java.io.File;

import butterknife.BindView;

/**
 * Created by Mukesh on 03/06/2016.
 */
public class ImageFragment extends BaseFragment {

    public static final String BUNDER_EXTRAS_IMAGE = "image";

    @BindView(R.id.zdvImage) ZoomableDraweeView zdvImage;

    public static ImageFragment newInstance(String image, boolean isLocalImage) {
        ImageFragment imageFragment = new ImageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDER_EXTRAS_IMAGE, image);
        bundle.putBoolean(ImagePreviewDialogFragment.BUNDLE_EXTRAS_IS_LOCAL_IMAGE, isLocalImage);
        imageFragment.setArguments(bundle);
        return imageFragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_image;
    }

    @Override
    public void init() {
        // get arguments
        if (null != getArguments()) {
            String image = getArguments().getString(BUNDER_EXTRAS_IMAGE);
            image = null != image ? image : "";

            // check if image is a local file or url
            if (image.contains("http")) {
                setImage(GeneralFunctions.getResizedImageUri(image, 0, 0));
            } else {
                File imageFile = new File(image);
                if (imageFile.exists()) {
                    setImage(GeneralFunctions.getLocalImageUri(imageFile));
                }
            }
//            if (getArguments().getBoolean(ImagePreviewDialogFragment.BUNDLE_EXTRAS_IS_LOCAL_IMAGE,
//                    false)) {
//                File imageFile = new File(image);
//                if (imageFile.exists()) {
//                    setImage(GeneralFunctions.getLocalImageUri(imageFile));
//                }
//            } else {
//                setImage(GeneralFunctions.getResizedImageUri(image, 0, 0));
//            }
        }
    }

    @Override
    public BasePresenter getPresenter() {
        return null;
    }

    private void setImage(Uri uri) {
        if (null != uri) {
            zdvImage.setAllowTouchInterceptionWhileZoomed(true);
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setUri(uri)
                    .build();
            zdvImage.setController(controller);
        }
    }
}
