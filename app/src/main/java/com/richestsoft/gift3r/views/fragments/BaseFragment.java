package com.richestsoft.gift3r.views.fragments;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.preferences.UserPrefsManager;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.utils.MyCustomLoader;
import com.richestsoft.gift3r.views.activities.BaseAppCompatActivity;
import com.richestsoft.gift3r.views.activities.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * Created by Mukesh on 12/08/2016.
 */
public abstract class BaseFragment extends Fragment {

    @Nullable @BindView(R.id.toolbar) Toolbar toolbar;
    @Nullable @BindView(R.id.tvTitle) TextView tvTitle;

    private Unbinder mUnbinder;
    private MyCustomLoader mMyCustomLoader;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUnbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMyCustomLoader = ((BaseAppCompatActivity) getActivity()).getMyCustomLoader();

        if (null != toolbar) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().popBackStackImmediate();
                }
            });
        }
        init();
    }

    public Context getActivityContext() {
        return getActivity();
    }

    public UserPrefsManager getUserPrefsManager() {
        return ((BaseAppCompatActivity) getActivity()).getUserPrefsManager();
    }

    public void showProgressLoader() {
        if (null != mMyCustomLoader) {
            mMyCustomLoader.showProgressDialog(getString(R.string.loading));
        }
    }

    public void hideProgressLoader() {
        if (null != mMyCustomLoader) {
            mMyCustomLoader.dismissProgressDialog();
        }
    }

    public void expireUserSession() {
        hideProgressLoader();
        getUserPrefsManager().clearUserPrefs();
        showMessage(R.string.session_expired, null, false);
        startActivity(new Intent(getActivity(), MainActivity.class));
        getActivity().finish();
    }

    public void showMessage(int resId, String message, boolean isShowSnackbarMessage) {
        if (null != mMyCustomLoader && !isFragmentDestroyed()) {
            if (isShowSnackbarMessage) {
                mMyCustomLoader.showSnackBar(getView(), null != message ? message :
                        getString(resId));
            } else {
                mMyCustomLoader.showToast(null != message ? message : getString(resId));
            }
        }
    }

    public void dismissDialogFragment() {
        DialogFragment dialogFragment = (DialogFragment) getFragmentManager()
                .findFragmentByTag(getString(R.string.dialog));
        if (null != dialogFragment) {
            dialogFragment.dismiss();
        }
    }

    public boolean isFragmentDestroyed() {
        return null == getActivity() || !isAdded();
    }

    @Override
    public void onDestroyView() {
        mUnbinder.unbind();
        mMyCustomLoader = null;
        if (null != getPresenter()) {
            getPresenter().detachView();
        }
        super.onDestroyView();
    }

    public abstract int getLayoutId();

    public abstract void init();

    public abstract BasePresenter getPresenter();

}
