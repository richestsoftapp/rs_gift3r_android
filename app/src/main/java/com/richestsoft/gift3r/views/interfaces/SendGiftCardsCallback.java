package com.richestsoft.gift3r.views.interfaces;

/**
 * Created by user28 on 23/2/18.
 */

public interface SendGiftCardsCallback {

    void sendGiftCard(Integer cardId,String mypurchasedcard_id,String message);

}
