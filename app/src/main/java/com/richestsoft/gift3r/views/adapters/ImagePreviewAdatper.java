package com.richestsoft.gift3r.views.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.richestsoft.gift3r.views.fragments.ImageFragment;

import java.util.List;

/**
 * Created by Mukesh on 24/05/2016.
 */
public class ImagePreviewAdatper extends FragmentStatePagerAdapter {

    private List<String> imagesList;
    private boolean isLocalImage;

    public ImagePreviewAdatper(FragmentManager fm, List<String> imagesList, boolean isLocalImage) {
        super(fm);
        this.imagesList = imagesList;
        this.isLocalImage = isLocalImage;
    }

    @Override
    public Fragment getItem(int position) {
        return ImageFragment.newInstance(imagesList.get(position), isLocalImage);
    }

    @Override
    public int getCount() {
        return imagesList.size();
    }
}
