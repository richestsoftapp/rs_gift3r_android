package com.richestsoft.gift3r.views.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.models.pojos.Favouritelist;
import com.richestsoft.gift3r.models.pojos.Payment_Request;
import com.richestsoft.gift3r.models.pojos.Payment_Response;
import com.richestsoft.gift3r.models.pojos.PojoCommon;
import com.richestsoft.gift3r.models.pojos.Store;
import com.richestsoft.gift3r.utils.ApplicationGlobal;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.utils.ScreenDimensions;
import com.richestsoft.gift3r.views.activities.HomeActivity;
import com.richestsoft.gift3r.views.activities.MainActivity;
import com.richestsoft.gift3r.views.fragments.StoreDetailFragment;
import com.richestsoft.gift3r.views.interfaces.BuyGiftCardCallBacks;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by user28 on 9/2/18.
 */

public class StoreDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int ROW_TYPE_HEADER = 1;
    private final int ROW_TYPE_CHILD = 2;
    android.support.v7.app.AlertDialog alertDialog;
    private LayoutInflater mLayoutInflater;
    private Fragment mFragment;
    String userChoosenTask;
    String storeid;
    private Store mStore;
    Favouritelist favouritelist;
    private BuyGiftCardCallBacks mBuyGiftCardCallBacks;
    private int imageWidth, imageHeight;
    String selectedSuperStar;
    int current_year, max_year;
    int month;
    int current_month;
    public static boolean yes = false;
    public static boolean buy = false;
    ArrayList<String> list_of_month = new ArrayList<>();
    ArrayList<String> list_of_year = new ArrayList<>();
    public int mm_position;
    public int yy_position;
    private String selected_mm;
    private String selected_yy;
    public static String SelectedAmount;

    public StoreDetailAdapter(Fragment fragment, Store store, Favouritelist favouritelist) {
        mFragment = fragment;
        this.favouritelist = favouritelist;
        mStore = store;
        mBuyGiftCardCallBacks = (BuyGiftCardCallBacks) fragment;
        mLayoutInflater = LayoutInflater.from(fragment.getActivity());
        imageWidth = new ScreenDimensions(fragment.getActivity()).getScreenWidth();
        imageHeight = (int) mFragment.getResources().getDimension(R.dimen.store_Detail_image_height);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (ROW_TYPE_HEADER == viewType) {
            return new StoreViewHolder(mLayoutInflater.inflate(R.layout.row_store_detail_header, parent, false));
        } else {
            return new CardViewHolder(mLayoutInflater.inflate
                    (R.layout.row_store_detail_child, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

     //   if (StoreDetailFragment.searchid.equals("1")) {

            if (ROW_TYPE_HEADER == getItemViewType(position)) {
                StoreViewHolder storeViewHolder = (StoreViewHolder) holder;
                //     Log.e("status ",String.valueOf(mStore.getStatus()));
                storeViewHolder.tv_noavailable.setVisibility(View.VISIBLE);

                if (mStore.getStatus() == 1) {
                    storeViewHolder.tvfavorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.giftred2, 0, 0, 0);
                } else {
                    storeViewHolder.tvfavorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.giftgray2, 0, 0, 0);
                }

                //    storeViewHolder.tvAddress.setText(mStore.getAddress());
                //    storeViewHolder.tvPhoneNo.setText(mStore.getPhone_no());
                // add https:// at the start of url
                if (!mStore.getWebsite().startsWith("http://") && !mStore.getWebsite().startsWith("https://")) mStore.setWebsite("https://" + mStore.getWebsite());
                //    storeViewHolder.tvWebsite.setText(mStore.getWebsite());

               // storeViewHolder.sdvStoreImage.setImageURI(GeneralFunctions.getResizedImageUri(mStore.getImage(), imageWidth, imageHeight));
               // storeViewHolder.sdvStoreImage.setImageURI(mStore.getImage());
                Picasso.get()
                        .load(mStore.getImage())
                        .placeholder(R.color.colorPlaceHolderImageDark)
                        .into(storeViewHolder.sdvStoreImage2);
            } else {
                CardViewHolder cardViewHolder = (CardViewHolder) holder;

/*
                if (mStore.getAvailable_cards().get(position).getMessage()==null || mStore.getAvailable_cards().get(position).getMessage().equalsIgnoreCase("")){
                    cardViewHolder.ivCardmessage.setImageResource(R.drawable.messageicon_red);
                }else {
                    cardViewHolder.ivCardmessage.setImageResource(R.drawable.messageicon);
                }*/
                // subtracting 1 from position to neutralize +1 in getItemCount
                cardViewHolder.tvCardName.setText(mStore.getAvailable_cards().get(position - 1).getGift_card_name());

                Log.e("amount is",String.valueOf(mStore.getAvailable_cards().get(position - 1).getPrice()));
                cardViewHolder.tvPrice.setText(String.format(mFragment.getString(R.string.price_format), mStore.getAvailable_cards().get(position - 1).getPrice()));

            }


 //       } else {

/*
            if (ROW_TYPE_HEADER == getItemViewType(position)) {
                StoreViewHolder storeViewHolder = (StoreViewHolder) holder;

                storeViewHolder.tv_noavailable.setVisibility(View.GONE);

                //     Log.e("status ",String.valueOf(mStore.getStatus()));

                if (favouritelist.getStatus() == 1) {

                    storeViewHolder.tvfavorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.giftred2, 0, 0, 0);
                } else {

                    storeViewHolder.tvfavorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.giftgray2, 0, 0, 0);
                }

                //    storeViewHolder.tvAddress.setText(mStore.getAddress());
                //    storeViewHolder.tvPhoneNo.setText(mStore.getPhone_no());
                // add https:// at the start of url
                if (!favouritelist.getWebsite().startsWith("http://") && !favouritelist.getWebsite().startsWith("https://"))
                    favouritelist.setWebsite("https://" + favouritelist.getWebsite());
                //    storeViewHolder.tvWebsite.setText(mStore.getWebsite());

               // storeViewHolder.sdvStoreImage.setImageURI(GeneralFunctions.getResizedImageUri(favouritelist.getImage(), imageWidth, imageHeight));
                //storeViewHolder.sdvStoreImage.setImageURI(favouritelist.getImage());

                Picasso.get()
                        .load(favouritelist.getImage())
                        .placeholder(R.color.colorPlaceHolderImageDark)
                        .into(storeViewHolder.sdvStoreImage2);
            } else {
                CardViewHolder cardViewHolder = (CardViewHolder) holder;
                // subtracting 1 from position to neutralize +1 in getItemCount
                cardViewHolder.tvCardName.setText(mStore.getAvailable_cards().get(position - 1).getGift_card_name());
                cardViewHolder.tvPrice.setText(String.format(mFragment.getString(R.string.price_format), mStore.getAvailable_cards().get(position - 1).getPrice()));
            }*/

 //       }

    }

    @Override
    public int getItemViewType(int position) {
        if (0 == position) {
            return ROW_TYPE_HEADER;
        } else {
            return ROW_TYPE_CHILD;
        }
    }

    @Override
    public int getItemCount() {
        // retuning one more than size to inflate header(store) row

        return mStore.getAvailable_cards().size() + 1;

      /*  if (StoreDetailFragment.searchid.equals("1")) {
            return mStore.getAvailable_cards().size() + 1;
        } else {
            return 1;
        }*/
    }

    class StoreViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.sdvStoreImage)
        SimpleDraweeView sdvStoreImage;

        @BindView(R.id.sdvStoreImage2)
        ImageView sdvStoreImage2;

        @BindView(R.id.tvPhoneNo)
        TextView tvPhoneNo;
        @BindView(R.id.tv_noavailable)
        TextView tv_noavailable;
        @BindView(R.id.tvWebsite)
        TextView tvWebsite;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.tvfavorite)
        TextView tvfavorite;

        // @BindView(R.id.tvAddress) TextView tvAddress;

        StoreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.tvWebsite, R.id.tvPhoneNo, R.id.tvfavorite, R.id.tvAddress})
        public void onClicked(View v) {
            switch (v.getId()) {
                case R.id.tvWebsite:

                    mFragment.getActivity().startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(mStore.getWebsite())));

                 /*   if (StoreDetailFragment.searchid.equals("1")) {
                        mFragment.getActivity().startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse(mStore.getWebsite())));
                    } else {
                        *//*mFragment.getActivity().startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse(favouritelist.getWebsite())));*//*
                    }*/

                    break;

                case R.id.tvPhoneNo:
                    AlertDialog.Builder builder = new AlertDialog.Builder(mFragment.getActivity());
                    builder.setMessage("Would you like to call this number " + mStore.getPhone_no())
                            .setCancelable(true)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    mFragment.getActivity().startActivity(new Intent(Intent.ACTION_DIAL,
                                            Uri.parse("tel:" + mStore.getPhone_no())));
                                 /*   if (StoreDetailFragment.searchid.equals("1")) {
                                        mFragment.getActivity().startActivity(new Intent(Intent.ACTION_DIAL,
                                                Uri.parse("tel:" + mStore.getPhone_no())));
                                    } else {
                                    *//*    mFragment.getActivity().startActivity(new Intent(Intent.ACTION_DIAL,
                                                Uri.parse("tel:" + favouritelist.getPhoneNumber())));*//*
                                    }*/

                                    dialog.dismiss();
                                }
                            });

                    AlertDialog alert = builder.create();
                    alert.show();

                    //   Call_Dialog(m);


                    break;
                case R.id.tvfavorite:

                    storeid = String.valueOf(mStore.getStore_id());
                 /*   if (StoreDetailFragment.searchid.equals("1")) {

                        storeid = String.valueOf(mStore.getStore_id());

                    } else {
                     //   storeid = favouritelist.getSId();
                    }
*/
                    addToFavourites(tvfavorite);
                    //   Toast.makeText(mFragment.getActivity(), "favorite clicked", Toast.LENGTH_SHORT).show();
                 /*   mFragment.getActivity().startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(mStore.getWebsite())));*/
                    break;

                case R.id.tvAddress:

                    String uri = String.format("http://maps.google.co.in/maps?q=" + mStore.getAddress());
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    mFragment.getActivity().startActivity(intent);

                  /*  if (StoreDetailFragment.searchid.equals("1")) {
                        String uri = String.format("http://maps.google.co.in/maps?q=" + mStore.getAddress());
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        mFragment.getActivity().startActivity(intent);
                    } else {
                      *//*  String uri = String.format("http://maps.google.co.in/maps?q=" + favouritelist.getAddress());
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        mFragment.getActivity().startActivity(intent);*//*
                    }*/


                    //   Toast.makeText(mFragment.getActivity(), "address clicked", Toast.LENGTH_SHORT).show();
                 /*   mFragment.getActivity().startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(mStore.getWebsite())));*/
                    break;
            }
        }

    }

    class CardViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvCardName)
        TextView tvCardName;
        @BindView(R.id.tvPrice)
        TextView tvPrice;
        @BindView(R.id.ivCardmessage)
        ImageView ivCardmessage;

        CardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.btnBuy, R.id.btnSend, R.id.ivCardInfo, R.id.ivCardmessage})
        public void onClicked(View view) {
            switch (view.getId()) {
                case R.id.btnBuy:
                    SelectOption(getAdapterPosition());
                    //   mBuyGiftCardCallBacks.buyGiftCard(mStore.getAvailable_cards().get(getAdapterPosition() - 1).getGift_card_id(), "");
                    break;

                case R.id.ivCardmessage:

                    try {
                        if (mStore.getAvailable_cards().get(getAdapterPosition()).getMessage()==null || mStore.getAvailable_cards().get(getAdapterPosition()).getMessage().equalsIgnoreCase("")){
                            Toast.makeText(mFragment.getActivity(), "No Message Found!", Toast.LENGTH_SHORT).show();
                        }else {
                            WriteNotePopUp(getAdapterPosition(),"","read");
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;

                case R.id.btnSend:

                    if (StoreDetailFragment.searchid.equalsIgnoreCase("3")){

                        WriteNotePopUp(getAdapterPosition(),"friend","write");

                    }else {
                        SelectOption(getAdapterPosition());
                    //    mBuyGiftCardCallBacks.sendGiftCard(mStore.getAvailable_cards().get(position - 1).getGift_card_id(), mStore.getAvailable_cards().get(position - 1).getMypurchasedcard_id(), "");
                    }


                    // mBuyGiftCardCallBacks.sendGiftCard(mStore.getAvailable_cards().get(getAdapterPosition() - 1).getGift_card_id());
                    break;
                case R.id.ivCardInfo:
                    AlertDialog.Builder builder = new AlertDialog.Builder(mFragment.getActivity());
                    builder.setMessage(R.string.cards_disclaimer)
                            .setCancelable(true)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                    break;
            }
        }
    }

    private void addToFavourites(final TextView tvfavorite) {

        StoreDetailFragment.rl_prog.setVisibility(View.VISIBLE);
        RestClient.get().AddFavorite(ApplicationGlobal.getSessionId(), storeid)
                .enqueue(new Callback<PojoCommon>() {
                    @Override
                    public void onResponse(Call<PojoCommon> call,
                                           retrofit2.Response<PojoCommon> response) {
                        StoreDetailFragment.rl_prog.setVisibility(View.GONE);
                        try {

                            if (response.body().getStatus() == 1) {

                                Toast.makeText(mFragment.getActivity(), "Successfully added in my restaurants list", Toast.LENGTH_SHORT).show();

                                tvfavorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.giftred2, 0, 0, 0);


                            } else {
                                Toast.makeText(mFragment.getActivity(), "Successfully removed from my restaurants list", Toast.LENGTH_SHORT).show();

                                tvfavorite.setCompoundDrawablesWithIntrinsicBounds(R.drawable.giftgray2, 0, 0, 0);


                            }


                            if (StoreDetailFragment.searchid.equalsIgnoreCase("1")){
                              /*  Intent intent=new Intent(mFragment.getActivity(), HomeActivity.class);
                                intent.putExtra("type","stores");
                                mFragment.getActivity().startActivity(intent);
                                mFragment.getActivity().finish();*/
                            } else if (StoreDetailFragment.searchid.equalsIgnoreCase("2")) {
                                Intent intent=new Intent(mFragment.getActivity(),HomeActivity.class);
                                intent.putExtra("type","registry");
                                mFragment.getActivity().startActivity(intent);
                                mFragment.getActivity().finish();
                            }else if (StoreDetailFragment.searchid.equalsIgnoreCase("3")) {
                                Intent intent=new Intent(mFragment.getActivity(),HomeActivity.class);
                                intent.putExtra("type","contact");
                                mFragment.getActivity().startActivity(intent);
                                mFragment.getActivity().finish();
                            }else {

                            }


                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }

                    @Override
                    public void onFailure(Call<PojoCommon> call, Throwable t) {
                        //   ivFavourite.setEnabled(true);
                        //    myCustomLoader.hidedialog();
                        //      myCustomLoader.handleRetrofitError(mFragment.getView(), String.valueOf(t));
                    }
                });
    }

    private void SelectOption(final int position) {

        final Dialog dialog = new Dialog(mFragment.getActivity());
        dialog.setContentView(R.layout.sendoptiondialog);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        // set the custom dialog components - text, image and button
        Button btn_myself = (Button) dialog.findViewById(R.id.btn_myself);
        Button btn_afriend = (Button) dialog.findViewById(R.id.btn_afriend);

        if (StoreDetailFragment.searchid.equalsIgnoreCase("3")){
            btn_myself.setVisibility(View.GONE);
        }else {
            btn_myself.setVisibility(View.VISIBLE);
        }


        btn_myself.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //    WriteNotePopUp(0,position);

                Show_Card_Detail_Popup("BUY", position,"");

                dialog.dismiss();
            }
        });

        btn_afriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SelectedAmount=String.valueOf(mStore.getAvailable_cards().get(position - 1).getPrice());

             //    Show_Card_Detail_Popup("FRIEND", position,"");
             //   WriteNotePopUp(position, "FRIEND","write");

            /*    if (StoreDetailFragment.searchid.equalsIgnoreCase("3")){

                    WriteNotePopUp(position,"friend","write");

                }else {*/
                    mBuyGiftCardCallBacks.sendGiftCard(mStore.getAvailable_cards().get(position - 1).getGift_card_id(), mStore.getAvailable_cards().get(position - 1).getMypurchasedcard_id(), "");
       //         }


                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void WriteNotePopUp(final int position, final String friend,final String type) {

        final Dialog dialog = new Dialog(mFragment.getActivity());
        dialog.setContentView(R.layout.writenotepopup);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        // set the custom dialog components - text, image and button
        Button btn_sendnote = (Button) dialog.findViewById(R.id.btn_sendnote);
        final EditText et_writenotes = (EditText) dialog.findViewById(R.id.et_writenotes);
        final TextView tv_readnote = (TextView) dialog.findViewById(R.id.tv_readnote);

       /* if (type.equalsIgnoreCase("read")){
            btn_sendnote.setVisibility(View.GONE);
            et_writenotes.setEnabled(false);
            et_writenotes.setFocusable(false);
            et_writenotes.setText(mStore.getAvailable_cards().get(position).getMessage());
            tv_readnote.setText("READ NOTE");
        }else {*/

            btn_sendnote.setVisibility(View.VISIBLE);
            et_writenotes.setEnabled(true);
            et_writenotes.setFocusable(true);
            tv_readnote.setText("WRITE A NOTE");
            btn_sendnote.setText("Save");

           /* if (friend.equalsIgnoreCase("FRIEND")){

                btn_sendnote.setText("Save");
            }else {
                btn_sendnote.setText("Send");
            }*/

  //      }


        btn_sendnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Show_Card_Detail_Popup("FRIEND", position,et_writenotes.getText().toString());
           /*     if (friend.equalsIgnoreCase("FRIEND")) {
                    Show_Card_Detail_Popup("FRIEND", position,et_writenotes.getText().toString());

                } else {


                }
*/
                dialog.dismiss();

            }
        });

        dialog.show();

    }


    public void Show_Card_Detail_Popup(final String type, final int pos, final String notes) {

        final Dialog dialog = new Dialog(mFragment.getActivity());
        dialog.setContentView(R.layout.card_detail_layout);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);


        // set the custom dialog components - text, image and button

/*
        Calendar c = Calendar.getInstance();
        current_year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);

        current_month = month + 1;
        for (int i = 0; i < 10; i++) {
            max_year = current_year + i;

        }*/

list_of_year.clear();
list_of_month.clear();
        Calendar c = Calendar.getInstance();
        current_year = c.get(Calendar.YEAR)-1;
        month = c.get(Calendar.MONTH);

        current_month = 0;
        for (int i = 0; i < 13; i++) {
            max_year = current_year + i;

           // list_of_month.add(String.valueOf(current_month + i));
            if (i ==0) {

                list_of_month.add(i,"MM");
                list_of_year.add(i,"YY");

            }else {

                if (i <= 9) {
                    list_of_month.add(i, "0" + String.valueOf(current_month + i));

                }else {
                    list_of_month.add(i, String.valueOf(current_month + i));
                }

                String substring = null;
                try {
                    substring = String.valueOf(current_year).substring(Math.max(String.valueOf(current_year).length() - 2, 0));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    list_of_year.add(i,String.valueOf(Integer.parseInt(substring) + i));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

            }
        }
        final RadioButton rb_ae, rb_visa, rb_discover, rb_mastercard, sheamus;
        Button btn_pay = (Button) dialog.findViewById(R.id.btn_pay);
        ImageView card_info = dialog.findViewById(R.id.card_info);
        ImageView back = (ImageView) dialog.findViewById(R.id.back);
        RelativeLayout ll_visa = dialog.findViewById(R.id.ll_visa);
        RelativeLayout ll_mastercard = dialog.findViewById(R.id.ll_mastercard);
        RelativeLayout ll_discover = dialog.findViewById(R.id.ll_discover);
        RelativeLayout ll_ae = dialog.findViewById(R.id.ll_ae);
        final EditText et_cardholdername = dialog.findViewById(R.id.et_cardholdername);
        final EditText et_cardnumber = dialog.findViewById(R.id.et_cardnumber);
        final EditText et_month = dialog.findViewById(R.id.et_month);
        final EditText et_cardsecondname = dialog.findViewById(R.id.et_cardsecondname);
        final EditText et_year = dialog.findViewById(R.id.et_year);
        final EditText et_cvv = dialog.findViewById(R.id.et_cvv);
        final TextView tv_amount = dialog.findViewById(R.id.tv_amount);
        final TextView tv_cardtype = dialog.findViewById(R.id.tv_cardtype);
        final RelativeLayout rl_cardprogress = dialog.findViewById(R.id.rl_cardprogress);
        Spinner spin_mm = (Spinner) dialog.findViewById(R.id.spn_mm);
        Spinner spin_yy = (Spinner) dialog.findViewById(R.id.spn_yy);
        //  radioGroup = (RadioGroup) dialog.findViewById(R.id.radiogroup);
        rb_ae = (RadioButton) dialog.findViewById(R.id.rb_ae);
        rb_visa = (RadioButton) dialog.findViewById(R.id.rb_visa);
        rb_discover = (RadioButton) dialog.findViewById(R.id.rb_discover);
        rb_mastercard = (RadioButton) dialog.findViewById(R.id.rb_mastercard);

        RadioGroup radioGroup;

        ArrayAdapter aa = new ArrayAdapter(mFragment.getActivity(), android.R.layout.simple_spinner_item, list_of_month);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Setting the ArrayAdapter data on the Spinner
        spin_mm.setAdapter(aa);
        selectedSuperStar = "Visa";
        rb_visa.setChecked(true);
        rb_mastercard.setChecked(false);
        rb_discover.setChecked(false);
        rb_ae.setChecked(false);

/*
        if (rb_visa.isChecked()) {

        } else if (rb_ae.isChecked()) {

        } else if (rb_mastercard.isChecked()) {

        } else if (rb_discover.isChecked()) {

        }
*/

        ll_visa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rb_visa.setChecked(true);
                rb_mastercard.setChecked(false);
                rb_discover.setChecked(false);
                rb_ae.setChecked(false);
                et_cvv.setText("");
                selectedSuperStar = "Visa";

                InputFilter[] FilterArray = new InputFilter[1];
                FilterArray[0] = new InputFilter.LengthFilter(3);
                et_cvv.setFilters(FilterArray);
            }
        });


        ll_mastercard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_visa.setChecked(false);
                rb_mastercard.setChecked(true);
                rb_discover.setChecked(false);
                rb_ae.setChecked(false);
                selectedSuperStar = "MasterCard";
                et_cvv.setText("");
                InputFilter[] FilterArray = new InputFilter[1];
                FilterArray[0] = new InputFilter.LengthFilter(3);
                et_cvv.setFilters(FilterArray);
            }
        });

        ll_discover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rb_visa.setChecked(false);
                rb_mastercard.setChecked(false);
                rb_discover.setChecked(true);
                rb_ae.setChecked(false);
                selectedSuperStar = "Discover";
                et_cvv.setText("");
                InputFilter[] FilterArray = new InputFilter[1];
                FilterArray[0] = new InputFilter.LengthFilter(3);
                et_cvv.setFilters(FilterArray);

            }
        });

        ll_ae.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_visa.setChecked(false);
                rb_mastercard.setChecked(false);
                rb_discover.setChecked(false);
                rb_ae.setChecked(true);
                selectedSuperStar = "American Express";
                et_cvv.setText("");
                InputFilter[] FilterArray = new InputFilter[1];
                FilterArray[0] = new InputFilter.LengthFilter(4);
                et_cvv.setFilters(FilterArray);
            }
        });

        spin_mm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mm_position=position;

               // Object kkk = parent.getItemAtPosition(position);

                 selected_mm=list_of_month.get(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter aaa = new ArrayAdapter(mFragment.getActivity(), android.R.layout.simple_spinner_item, list_of_year);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//      Setting the ArrayAdapter data on the Spinner
        spin_yy.setAdapter(aaa);

        spin_yy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yy_position=position;
                selected_yy=list_of_year.get(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

       // new DecimalFormat("#.##").format(mStore.getAvailable_cards().get(pos - 1).getPrice());

        // tv_amount.setText("$"+String.valueOf(mStore.getAvailable_cards().get(pos - 1).getPrice()));

        tv_amount.setText(String.format(mFragment.getString(R.string.price_format2), mStore.getAvailable_cards().get(pos - 1).getPrice()));
        tv_cardtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Select_Options(tv_cardtype);

            }
        });

        card_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            //    Toast.makeText(mFragment.getActivity(), "kjhdjkfhskd", Toast.LENGTH_SHORT).show();

                new AlertDialog.Builder(mFragment.getActivity())
                        .setTitle("About CVV")
                        .setMessage("3-digit security code usually found on the back of your card.American express cards have 4-digit code located on the front.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();


            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(et_cardholdername.getText().toString().trim())) {
                    Toast.makeText(mFragment.getActivity(), "Please enter First name", Toast.LENGTH_SHORT).show();
                } else   if (TextUtils.isEmpty(et_cardsecondname.getText().toString().trim())) {
                    Toast.makeText(mFragment.getActivity(), "Please enter Last name", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(et_cardnumber.getText().toString().trim())) {
                    Toast.makeText(mFragment.getActivity(), "Please enter card number", Toast.LENGTH_SHORT).show();
                } else if (et_cardnumber.getText().toString().trim().length()!=16) {
                    Toast.makeText(mFragment.getActivity(), "Card not Valid, please enter the correct card numbers", Toast.LENGTH_SHORT).show();
                }else if (mm_position==0) {
                    Toast.makeText(mFragment.getActivity(), "Please select month", Toast.LENGTH_SHORT).show();

               /* } else if (Integer.parseInt(et_month.getText().toString().trim()) < current_month || Integer.parseInt(et_month.getText().toString().trim()) > 12) {

                    Toast.makeText(mFragment.getActivity(), "Please enter valid month", Toast.LENGTH_SHORT).show();*/

                } else if (yy_position==0) {

                    Toast.makeText(mFragment.getActivity(), "Please select year", Toast.LENGTH_SHORT).show();

                }/* else if (Integer.parseInt(selected_yy) <= 31) {

                    Toast.makeText(mFragment.getActivity(), "Please  select valid year", Toast.LENGTH_SHORT).show();

                }*/ else if (TextUtils.isEmpty(et_cvv.getText().toString().trim())) {
                    Toast.makeText(mFragment.getActivity(), "Please enter cvv", Toast.LENGTH_SHORT).show();
                } else if (selectedSuperStar.equalsIgnoreCase("American Express") && et_cvv.getText().toString().length()<=3) {
                    Toast.makeText(mFragment.getActivity(), "Please enter valid cvv", Toast.LENGTH_SHORT).show();
                }else if (et_cvv.getText().toString().length()<=2) {
                    Toast.makeText(mFragment.getActivity(), "Please enter valid cvv", Toast.LENGTH_SHORT).show();
                }else {

                    Payment_Request payment_request = new Payment_Request();
                    payment_request.setAmount(String.valueOf(mStore.getAvailable_cards().get(pos-1).getPrice()));
                    payment_request.setCardHoldername(et_cardholdername.getText().toString().trim()+" "+et_cardsecondname.getText().toString().trim());
                    payment_request.setCardNumber(et_cardnumber.getText().toString().trim());
                    //payment_request.setCardType(tv_cardtype.getText().toString().trim());
                    payment_request.setCardType(selectedSuperStar);
                    payment_request.setCvv(et_cvv.getText().toString().trim());

      //              String substring = selected_yy.substring(Math.max(selected_yy.length() - 2, 0));

                    payment_request.setExpiryDate(selected_mm+selected_yy);
/*
                    if (Integer.parseInt(selected_mm) <= 9) {
                        payment_request.setExpiryDate(0 + selected_mm + substring);
                    } else {
                        payment_request.setExpiryDate(selected_mm + substring);
                    }*/
                    payment_request.setSessionId(ApplicationGlobal.getSessionId());
                    payment_Api(payment_request, dialog, pos, type, rl_cardprogress,notes);

                }
            }
        });

        dialog.show();

    }

    private void Select_Options(final TextView textView) {

        final CharSequence[] items = {"VISA", "mastercard", "American Express"};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mFragment.getActivity());
        builder.setTitle("  Select Card!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                textView.setText(items[item]);

            }
        });
        builder.show();
    }

    private void payment_Api(Payment_Request payment_request, final Dialog dialog, final int pos, final String type, final RelativeLayout rl_cardprogress, final String notes) {

        rl_cardprogress.setVisibility(View.VISIBLE);

        RestClient.get().Payment("application/json", payment_request)
                .enqueue(new Callback<Payment_Response>() {
                    @Override
                    public void onResponse(Call<Payment_Response> call,
                                           retrofit2.Response<Payment_Response> response) {
                        try {

                            rl_cardprogress.setVisibility(View.GONE);
                            dialog.dismiss();
                            if (response.body().getError() == false) {
/*
                                buy = true;

                                if (type.equals("BUY")) {

                                } else {
                              //      mBuyGiftCardCallBacks.sendGiftCard(mStore.getAvailable_cards().get(pos - 1).getGift_card_id(), mStore.getAvailable_cards().get(pos - 1).getMypurchasedcard_id(), notes);
                                  //  WriteNotePopUp(pos, "FRIEND");
                                }*/

                               if (StoreDetailFragment.searchid.equalsIgnoreCase("3")){
                                   mBuyGiftCardCallBacks.buyGiftCard(mStore.getAvailable_cards().get(pos - 1).getGift_card_id(), StoreDetailFragment.phonenumber, notes);
                               }else {
                                   mBuyGiftCardCallBacks.buyGiftCard(mStore.getAvailable_cards().get(pos - 1).getGift_card_id(), "", "");
                               }



                                Toast.makeText(mFragment.getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mFragment.getActivity(), response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            //           myCustomLoader.hidedialog();
                            e.printStackTrace();

                            //  myCustomLoader.showToast(mContext.getString(R.string.error_general));
                        }
                    }

                    @Override
                    public void onFailure(Call<Payment_Response> call, Throwable t) {
                        //   ivFavourite.setEnabled(true);
                        //    myCustomLoader.hidedialog();
                        //      myCustomLoader.handleRetrofitError(mFragment.getView(), String.valueOf(t));
                    }
                });
    }
}
