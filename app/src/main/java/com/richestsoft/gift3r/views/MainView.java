package com.richestsoft.gift3r.views;

import com.richestsoft.gift3r.models.preferences.UserPrefsManager;

/**
 * Created by user28 on 13/2/18.
 */

public interface MainView {

    void navigateToHomeScreen();

    void navigateToSignInScreen();

    UserPrefsManager getUserPrefsManager();

}
