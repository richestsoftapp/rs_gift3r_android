package com.richestsoft.gift3r.views;

import android.content.Context;

import com.richestsoft.gift3r.models.preferences.UserPrefsManager;

/**
 * Created by Mukesh on 05/12/2016.
 */

interface BaseView {

    UserPrefsManager getUserPrefsManager();

    Context getActivityContext();

    boolean isFragmentDestroyed();

    void showProgressLoader();

    void hideProgressLoader();

    void expireUserSession();

    void showMessage(int resId, String string, boolean isShowSnackbarMessage);

    void dismissDialogFragment();

}
