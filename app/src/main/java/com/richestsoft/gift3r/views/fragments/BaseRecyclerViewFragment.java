package com.richestsoft.gift3r.views.fragments;

import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.views.utils.SlidingRelativeLayout;
import com.richestsoft.gift3r.views.utils.itemdecorations.HorizontalDividerItemDecoration;

import butterknife.BindView;

/**
 * Created by Mukesh on 19/08/2016.
 */
public abstract class BaseRecyclerViewFragment extends BaseFragment {

    @Nullable
    @BindView(R.id.rootLayout)
    SlidingRelativeLayout rootLayout;
    @Nullable
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @Nullable
    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @Override
    public void init() {
        // Set SwipeRefreshLayout
        if (null != swipeRefreshLayout) {
            swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorAccent,
                    R.color.colorAccent, R.color.colorAccent);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    onPullDownToRefresh();
                }
            });
        }

        // Set RecyclerView
        if (null != getLayoutManager()) {
            recyclerView.setLayoutManager(getLayoutManager());
        } else {
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
        if (isShowRecyclerViewDivider()) {
            recyclerView.addItemDecoration(new HorizontalDividerItemDecoration
                    .Builder(getActivity())
                    .colorResId(R.color.colorDivider)
                    .sizeResId(R.dimen.divider_height).showLastDivider()
                    .margin(0, 0).build());
        }
        if (null != getRecyclerViewAdapter()) {
            recyclerView.setAdapter(getRecyclerViewAdapter());
        }
        setData();
    }

    public void showSwipeRefreshLoader() {
        if (null != swipeRefreshLayout) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    if (null != swipeRefreshLayout) {
                        swipeRefreshLayout.setRefreshing(true);
                    }
                }
            });
        }
    }

    public void hideSwipeRefreshLoader() {
        if (null != swipeRefreshLayout && swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void showNoDataText(int resId, String message) {
        if (null != tvNoData) {
            tvNoData.setVisibility(View.VISIBLE);
            if (null != message) {
                tvNoData.setText(message);
            } else {
                tvNoData.setText(getString(resId));
            }
        }
    }

    public void hideNoDataText() {
        if (null != tvNoData) {
            tvNoData.setVisibility(View.GONE);
        }
    }


    public abstract void setData();

    public abstract RecyclerView.LayoutManager getLayoutManager();

    public abstract RecyclerView.Adapter getRecyclerViewAdapter();

    public abstract boolean isShowRecyclerViewDivider();

    public abstract void onPullDownToRefresh();
}
