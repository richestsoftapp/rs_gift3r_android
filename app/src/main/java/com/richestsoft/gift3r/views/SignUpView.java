package com.richestsoft.gift3r.views;

/**
 * Created by user28 on 22/2/18.
 */

public interface SignUpView extends BaseView {

    void navigateToVerification();

}
