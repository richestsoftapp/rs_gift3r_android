package com.richestsoft.gift3r.views.fragments;

import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.models.pojos.Favouritelist;
import com.richestsoft.gift3r.models.pojos.MyFavourites_Response;
import com.richestsoft.gift3r.models.pojos.PojoCommon;
import com.richestsoft.gift3r.models.pojos.Store;
import com.richestsoft.gift3r.models.pojos.User_Favorite_List;
import com.richestsoft.gift3r.utils.ApplicationGlobal;
import com.richestsoft.gift3r.views.adapters.MyRegistry_Adapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class MyRegistry extends Fragment {

    View view;
    RecyclerView rv_myRegistry;
    LinearLayoutManager manager;
    MyRegistry_Adapter myRegistry_adapter;
    List<Store> favouritelists=new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;
    TextView tvNoData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_my_registry, container, false);
        init();
        return view;
    }

    public void init(){
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        rv_myRegistry=view.findViewById(R.id.rv_myRegistry);
        tvNoData=view.findViewById(R.id.tvNoData);
        manager=new GridLayoutManager(getActivity(), 2);
        rv_myRegistry.setLayoutManager(manager);

        // implement setOnRefreshListener event on SwipeRefreshLayout
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // cancel the Visual indication of a refresh

                MyFavourites_List();

            }
        });

        MyFavourites_List();

    }

    private void MyFavourites_List() {
        //    ivFavourite.setEnabled(false);
        //     myCustomLoader.showProgressDialog();
        RestClient.get().My_favourites(ApplicationGlobal.getSessionId())
                .enqueue(new Callback<List<Store>>() {
                    @Override
                    public void onResponse(Call<List<Store>> call,
                                           retrofit2.Response<List<Store>> response) {
                        swipeRefreshLayout.setRefreshing(false);
                        if (response.code()== 400) {

                            rv_myRegistry.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.VISIBLE);
                            tvNoData.setText("No restaurant found");

                        } else{
                            tvNoData.setVisibility(View.GONE);
                            rv_myRegistry.setVisibility(View.VISIBLE);
                            try {
                                favouritelists = response.body();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            manager=new GridLayoutManager(getActivity(), 2);
                            rv_myRegistry.setLayoutManager(manager);
                            myRegistry_adapter = new MyRegistry_Adapter(getActivity(), favouritelists,"registry");
                            rv_myRegistry.setAdapter(myRegistry_adapter);
                            myRegistry_adapter.notifyDataSetChanged();

                        }

                       /* try {
                            //   Toast.makeText(mFragment.getActivity(), String.valueOf(response.body().getStatus()), Toast.LENGTH_SHORT).show();

                            if (response.code()== 400) {
                                myRegistry_adapter.notifyDataSetChanged();

                            } else{

                                favouritelists = response.body().getFavouritelist();
                                manager=new GridLayoutManager(getActivity(), 2);
                                rv_myRegistry.setLayoutManager(manager);
                            myRegistry_adapter = new MyRegistry_Adapter(getActivity(), favouritelists);
                            rv_myRegistry.setAdapter(myRegistry_adapter);

                            myRegistry_adapter.notifyDataSetChanged();
                        }

                        } catch (Exception e) {
                            //           myCustomLoader.hidedialog();
                            e.printStackTrace();

                            //  myCustomLoader.showToast(mContext.getString(R.string.error_general));
                        }*/
                    }

                    @Override
                    public void onFailure(Call<List<Store>> call, Throwable t) {
                        swipeRefreshLayout.setRefreshing(false);
                        //   ivFavourite.setEnabled(true);
                        //    myCustomLoader.hidedialog();
                        //      myCustomLoader.handleRetrofitError(mFragment.getView(), String.valueOf(t));
                    }
                });
    }
}
