package com.richestsoft.gift3r.views.adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.models.pojos.Contact_Status_Response;
import com.richestsoft.gift3r.models.pojos.DataFromDbpojo;
import com.richestsoft.gift3r.models.pojos.Invite_Request;
import com.richestsoft.gift3r.models.pojos.Invite_Response;
import com.richestsoft.gift3r.models.pojos.Userdetail;
import com.richestsoft.gift3r.utils.ApplicationGlobal;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.activities.HomeActivity;
import com.richestsoft.gift3r.views.fragments.UserProfileFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class All_Contacts_Adapter extends RecyclerView.Adapter<All_Contacts_Adapter.MyViewHolder> {

    Uri imgUri;
    Contact_Status_Response contact_status_response;
    List<Userdetail> usercontactdetail=new ArrayList<>();
    List<Userdetail> arraylist=new ArrayList<>();
    Context context;
    List<DataFromDbpojo> alldatalist = new ArrayList();
    public static String Selectednumber;
    RecyclerView rv_select_category;

    public All_Contacts_Adapter(Context context, List<Userdetail> pag_usercontactdetail, List<Userdetail> usercontactdetail) {
       // this.contact_status_response = contact_status_response;
        this.usercontactdetail = pag_usercontactdetail;
        this.context = context;
        this.arraylist.addAll(usercontactdetail);

    /*    helper=new MyDbAdapter(context);
        alldatalist=helper.getData();*/

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // infalte the item Layout
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contactlistitem, viewGroup, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, final int position) {

        try {


            // Log.e("app ids are",app_preference.getUser_id()+"........"+chat_responses.get(position).getUserid());


            myViewHolder.btn_invite.setOnClickListener(new View.OnClickListener() {
                private String number;

                @Override
                public void onClick(View v) {
                    hideSoftKeyboard();
                    //Toast.makeText(context, "postion" + position, Toast.LENGTH_SHORT).show();
                    number = usercontactdetail.get(position).getC_number();
              //      Log.e("invited number is",number);
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("https://www.gift3rapp.com/apk/gift3r.apk")
                            .setCancelable(true)
                            .setTitle("Alert")
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    StoreDetailAdapter.yes=true;

                                    Invite_Request invite_request = new Invite_Request();
                                    invite_request.setPhone_number(number);
                                    invite_request.setSession_id(ApplicationGlobal.getSessionId());

                                    RestClient.get().invite_method("application/json", invite_request)
                                            .enqueue(new Callback<Invite_Response>() {
                                                @Override
                                                public void onResponse(Call<Invite_Response> call,
                                                                       retrofit2.Response<Invite_Response> response) {
                                                    if (response.code()==200) {
                                                        // Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                                        Toast.makeText(context, "An invitation link has been sent to"+" "+ number, Toast.LENGTH_SHORT).show();

                                                    }else {

                                                        Toast.makeText(context, "Something went wrong ! Plaese try again. ", Toast.LENGTH_SHORT).show();

                                                    }

                                                }

                                                @Override
                                                public void onFailure(Call<Invite_Response> call, Throwable t) {
                                                    t.printStackTrace();
                                                }
                                            });

                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                }

            });

            myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    hideSoftKeyboard();

               //     Log.e("selected number is",usercontactdetail.get(position).getC_number());
                    Selectednumber = usercontactdetail.get(position).getC_number();

                    if (myViewHolder.btn_invite.getVisibility() == View.VISIBLE) {

                    } else {

                        FragmentManager manager = ((Activity) context).getFragmentManager();
                        GeneralFunctions.addFragment(manager, R.animator.slide_right_in, 0, 0, R.animator.slide_right_out, new UserProfileFragment(), null, R.id.flFragmentContainerHome);

                    }
                }
            });

            if (usercontactdetail.get(position).getStatus() == 0) {
                myViewHolder.btn_invite.setVisibility(View.VISIBLE);
            } else {
                myViewHolder.btn_invite.setVisibility(View.GONE);
            }

            myViewHolder.tv_contactname.setText(usercontactdetail.get(position).getC_name());
            myViewHolder.tv_number.setText(usercontactdetail.get(position).getC_number());

           /* if (usercontactdetail.get(position).getC_photo()==null ||usercontactdetail.get(position).getC_photo().equals("")) {
               // imgUri = Uri.parse("content:com.android.contacts/display_photo/1");
            } else {
                Log.e("user profile uis",usercontactdetail.get(position).getC_photo());
                myViewHolder.civ_contactimage.setImageURI(Uri.parse(usercontactdetail.get(position).getC_photo()));
            }
*/


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
       // return alldatalist.size();
        return usercontactdetail.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_contactname, tv_number;
        CircleImageView civ_contactimage;
        Button btn_invite;


        public MyViewHolder(View itemView) {
            super(itemView);

            tv_contactname = itemView.findViewById(R.id.tv_contactname);
            tv_number = itemView.findViewById(R.id.tv_number);
            civ_contactimage = itemView.findViewById(R.id.civ_contactimage);
            btn_invite = itemView.findViewById(R.id.btn_invite);

        }
    }

    public void filter(String charText) {
      //  Log.e("searching text",charText);
        charText = charText.toLowerCase(Locale.getDefault());
        usercontactdetail.clear();
        if (charText.length() == 0) {
      //      Log.e("searching text","if part");
            usercontactdetail.addAll(arraylist);
        } else {
         //   Log.e("searching text",String.valueOf(arraylist.size()));
            for (Userdetail wp : arraylist) {
          //      Log.e("searching text","loop");
                if (wp.getC_name().toLowerCase(Locale.getDefault()).contains(charText) || wp.getC_number().toLowerCase(Locale.getDefault()).contains(charText)) {
                    usercontactdetail.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
    public void hideSoftKeyboard() {
        if(((HomeActivity)context).getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(((HomeActivity)context).getCurrentFocus().getWindowToken(), 0);
        }
    }
}
