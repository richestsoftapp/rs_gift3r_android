package com.richestsoft.gift3r.views.fragments;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.pojos.GiftCard;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.presenters.MyCardsPresenter;
import com.richestsoft.gift3r.views.MyCardsView;
import com.richestsoft.gift3r.views.adapters.MyCardsListAdapter;
import com.richestsoft.gift3r.views.interfaces.SendGiftCardsCallback;

import java.util.ArrayList;

import butterknife.BindView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by user28 on 9/2/18.
 */

public class MyCardsFragment extends BaseRecyclerViewFragment implements MyCardsView, SendGiftCardsCallback {

    @BindView(R.id.searchView) SearchView searchView;
    public static final String INTENT_FILTER_UPDATE_MY_CARD_LIST = "updateMyCards";
    public final int RESULT_PICK_CONTACT = 21;
    private String searchKey = "";
    private Integer selectedCardId;
    private String purchasedcard_id;
    private String message;
    private MyCardsPresenter mMyCardsPresenter;
    private MyCardsListAdapter mMyCardsListAdapter;

    @Override public int getLayoutId() {
        return R.layout.fragment_my_cards;
    }

    @Override public void setData() {

        //initialize and attach presenter
        mMyCardsPresenter = new MyCardsPresenter();
        mMyCardsPresenter.attachView(this);

        // fetch cards list
        mMyCardsPresenter.fetchMyCardList(true, searchKey);

        searchView.setQueryHint(getString(R.string.my_cards_search_hint));
        // set query submit listener
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchKey = s;
                mMyCardsPresenter.fetchMyCardList(true, searchKey);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.isEmpty()) {
                    searchKey = "";
                    mMyCardsPresenter.fetchMyCardList(true, searchKey);
                }
                return false;
            }
        });

        // register broadcast receiver (triggered after a card is redeemed)
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(updateMyCardsBroadcastReceiver, new
                        IntentFilter(INTENT_FILTER_UPDATE_MY_CARD_LIST));
    }

    @Override public RecyclerView.LayoutManager getLayoutManager() {
        return null;
    }

    @Override public RecyclerView.Adapter getRecyclerViewAdapter() {
        if (null == mMyCardsListAdapter) {
            mMyCardsListAdapter = new MyCardsListAdapter(this);
        }
        return mMyCardsListAdapter;
    }

    @Override public void updateRecyclerViewData(ArrayList<?> dataList) {
        mMyCardsListAdapter.updateList((ArrayList<GiftCard>) dataList);
    }

    @Override public boolean isShowRecyclerViewDivider() {
        return false;
    }

    @Override public void onPullDownToRefresh() {
        mMyCardsPresenter.fetchMyCardList(false, searchKey);
    }

    @Override public BasePresenter getPresenter() {
        return mMyCardsPresenter;
    }

    private BroadcastReceiver updateMyCardsBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null != context && null != intent) {
                mMyCardsPresenter.fetchMyCardList(true, searchKey);
            }
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(updateMyCardsBroadcastReceiver);
    }

    @Override public void sendGiftCard(Integer cardId,String mypurchasedcard_id,String message) {
        selectedCardId = cardId;
        purchasedcard_id=mypurchasedcard_id;
        this.message=message;
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_PICK_CONTACT && resultCode == RESULT_OK) {
            Uri contactUri = data.getData();
            final Cursor cursor = getActivity().getContentResolver().query(contactUri, null, null, null, null);
            cursor.moveToFirst();
           // final int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            final String column = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

        //    Log.e("number is",column);

            mMyCardsPresenter.sendMyCard(selectedCardId,purchasedcard_id,String.valueOf(column),message);

        //    WriteNotePopUp(String.valueOf(column));

       /*     //show warning before sending card
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.card_send_warning_msg)
                    .setCancelable(true)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            mMyCardsPresenter.sendMyCard(selectedCardId,purchasedcard_id,column,message);
                            cursor.close();
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            cursor.close();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();*/


        }
    }

    @Override
    public void sendBroadcast() {
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
                new Intent(MyCardsFragment.INTENT_FILTER_UPDATE_MY_CARD_LIST));
    }



    private void WriteNotePopUp(final String phonenumber) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.writenotepopup);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        // set the custom dialog components - text, image and button
        Button btn_sendnote = (Button) dialog.findViewById(R.id.btn_sendnote);
        final EditText et_writenotes = (EditText) dialog.findViewById(R.id.et_writenotes);
        final TextView tv_readnote = (TextView) dialog.findViewById(R.id.tv_readnote);



            btn_sendnote.setVisibility(View.VISIBLE);
            et_writenotes.setEnabled(true);
            et_writenotes.setFocusable(true);
            tv_readnote.setText("WRITE A NOTE");


        btn_sendnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(et_writenotes.getText().toString().trim())){
                    Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                }else {

                    mMyCardsPresenter.sendMyCard(selectedCardId,purchasedcard_id,phonenumber,message);
                    dialog.dismiss();
                }



            }
        });

        dialog.show();

    }


}
