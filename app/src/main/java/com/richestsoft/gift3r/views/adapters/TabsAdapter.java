package com.richestsoft.gift3r.views.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import com.richestsoft.gift3r.models.pojos.Tab;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Mukesh on 04-05-2016.
 */
public class TabsAdapter extends FragmentPagerAdapter {

    private List<Tab> tabsList = new ArrayList<Tab>();

    public TabsAdapter(FragmentManager fm, List<Tab> tabsList) {
        super(fm);
        this.tabsList = tabsList;
    }

    @Override
    public Fragment getItem(int position) {
        return tabsList.get(position).getTabFragment();
    }

    @Override
    public int getCount() {
        return tabsList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabsList.get(position).isShowTabName() ? tabsList.get(position).getTabName() : "";
    }

}
