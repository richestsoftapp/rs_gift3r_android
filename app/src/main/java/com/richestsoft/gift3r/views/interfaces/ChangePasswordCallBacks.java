package com.richestsoft.gift3r.views.interfaces;

/**
 * Created by user28 on 9/2/18.
 */

public interface ChangePasswordCallBacks {

    void onChangePasswordButtonClick(String oldPassword, String newPassword);

}
