package com.richestsoft.gift3r.views.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.EditText;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.presenters.OtpVerificationPresenter;
import com.richestsoft.gift3r.views.OtpVerificationView;
import com.richestsoft.gift3r.views.activities.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by user28 on 8/2/18.
 */

public class OtpVerificationFragment extends BaseFragment implements OtpVerificationView {

    public static final String BUNDLE_EXTRAS_EMAIL = "email";
    public static final String BUNDLE_EXTRAS_PHONE_NUMBER = "phoneNumber";
    public static final String BUNDLE_EXTRAS_NAME = "name";
    public static final String BUNDLE_EXTRAS_COUNTRY_CODE = "countryCod";
    public static final String BUNDLE_EXTRAS_PASSWORD = "password";

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    @BindView(R.id.etOtp) EditText etOtp;

    @Override public int getLayoutId() {
        return R.layout.fragment_otp_verification;
    }

    private OtpVerificationPresenter mOtpVerificationPresenter;

    public static OtpVerificationFragment newInstance(String email, String password, String phoneNumber, String name, String countryCode) {
        OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
        Bundle bundle = new Bundle();

        bundle.putString(BUNDLE_EXTRAS_EMAIL, email);
        bundle.putString(BUNDLE_EXTRAS_PASSWORD, password);
        bundle.putString(BUNDLE_EXTRAS_PHONE_NUMBER, phoneNumber);
        bundle.putString(BUNDLE_EXTRAS_NAME, name);
        bundle.putString(BUNDLE_EXTRAS_COUNTRY_CODE, countryCode);

        otpVerificationFragment.setArguments(bundle);
        return otpVerificationFragment;
    }

    @Override public void init() {
        // formatting toolbar and set toolbar title
        tvTitle.setText(R.string.fragment_title_otp_verification);
        toolbar.setBackground(null);
        tvTitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black);

        checkAndRequestPermissions();

        //initialize and attach presenter
        mOtpVerificationPresenter = new OtpVerificationPresenter();
        mOtpVerificationPresenter.attachView(this);
    }

    @Override public BasePresenter getPresenter() {
        return mOtpVerificationPresenter;
    }

    @OnClick({R.id.btnSubmit})
    public void onClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
                if (null != getArguments()) {
                    mOtpVerificationPresenter.signUpUser(getArguments().getString(BUNDLE_EXTRAS_EMAIL), getArguments().getString(BUNDLE_EXTRAS_COUNTRY_CODE), getArguments().getString(BUNDLE_EXTRAS_PHONE_NUMBER), getArguments().getString(BUNDLE_EXTRAS_NAME), getArguments().getString(BUNDLE_EXTRAS_PASSWORD), etOtp.getText().toString().trim());
                }
                break;
        }
    }

    @Override public void navigateToHomeScreen() {
        startActivity(new Intent(getActivity(), HomeActivity.class));
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");

                etOtp.setText(message);
            }
        }
    };

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.SEND_SMS);
        int receiveSMS = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECEIVE_SMS);
        int readSMS = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(),
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
    }

}
