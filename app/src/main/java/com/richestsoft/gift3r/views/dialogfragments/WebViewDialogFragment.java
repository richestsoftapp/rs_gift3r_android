package com.richestsoft.gift3r.views.dialogfragments;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.richestsoft.gift3r.R;

import butterknife.BindView;

/**
 * Created by Mukesh on 14-07-2016.
 */
public class WebViewDialogFragment extends BaseDialogFragment {

    @BindView(R.id.webView) WebView webView;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    private static final String BUNDLE_EXTRAS_TITLE = "title";
    private static final String BUNDLE_EXTRAS_URL = "url";

    public static WebViewDialogFragment newInstance(String title, String url) {
        WebViewDialogFragment webViewFragment = new WebViewDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_EXTRAS_TITLE, title);
        bundle.putString(BUNDLE_EXTRAS_URL, url);
        webViewFragment.setArguments(bundle);
        return webViewFragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialogfragment_webview;
    }

    @Override
    public void init() {
        // get arguments
        if (null != getArguments()) {
            tvTitle.setText(getArguments().getString(BUNDLE_EXTRAS_TITLE, ""));
            webView.loadUrl(getArguments().getString(BUNDLE_EXTRAS_URL, ""));
        }

        // enable javascript
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        // set webview client listener
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                if (null != progressBar) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public boolean getIsFullScreenDialog() {
        return true;
    }

    @Override
    public void onPause() {
        webView.onPause();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        webView.onResume();
    }

    @Override
    public void onDestroyView() {
        webView.destroy();
        webView = null;
        super.onDestroyView();
    }
}

