package com.richestsoft.gift3r.views.dialogfragments;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.views.adapters.ImagePreviewAdatper;
import com.richestsoft.gift3r.views.utils.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Mukesh on 03-06-2016.
 */
public class ImagePreviewDialogFragment extends BaseDialogFragment {

    public static final String BUNDLE_EXTRAS_IMAGES_LIST = "imagesList";
    public static final String BUNDLE_EXTRAS_IMAGE_POSITION = "imagePosition";
    public static final String BUNDLE_EXTRAS_IS_LOCAL_IMAGE = "isLocalImage";

    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.circlePagerIndicator) CirclePageIndicator circlePagerIndicator;

    public static ImagePreviewDialogFragment newInstance(List<String> imagesList, int position,
                                                         boolean isLocalImage) {
        ImagePreviewDialogFragment imagePreviewDialogFragment = new ImagePreviewDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(BUNDLE_EXTRAS_IMAGES_LIST, (ArrayList<String>) imagesList);
        bundle.putInt(BUNDLE_EXTRAS_IMAGE_POSITION, position);
        bundle.putBoolean(BUNDLE_EXTRAS_IS_LOCAL_IMAGE, isLocalImage);
        imagePreviewDialogFragment.setArguments(bundle);
        return imagePreviewDialogFragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialogfragment_image_preview;
    }

    @Override
    public void init() {
        // get bundle data from arguments
        if (null != getArguments()) {
            List<String> imagesList = getArguments().getStringArrayList(BUNDLE_EXTRAS_IMAGES_LIST);

            if (null != imagesList) {
                viewPager.setAdapter(new ImagePreviewAdatper(getChildFragmentManager(), imagesList,
                        getArguments().getBoolean(BUNDLE_EXTRAS_IS_LOCAL_IMAGE, false)));

                if (1 == imagesList.size()) {
                    circlePagerIndicator.setVisibility(View.GONE);
                } else {
                    circlePagerIndicator.setVisibility(View.VISIBLE);
                    circlePagerIndicator.setViewPager(viewPager);
                    viewPager.setCurrentItem(getArguments().getInt(BUNDLE_EXTRAS_IMAGE_POSITION, 0));
                }
            }
        }
    }

    @Override
    public boolean getIsFullScreenDialog() {
        return true;
    }
}
