package com.richestsoft.gift3r.views.dialogfragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.pojos.Country;
import com.richestsoft.gift3r.views.adapters.CountriesListAdapter;
import com.richestsoft.gift3r.views.utils.itemdecorations.HorizontalDividerItemDecoration;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Mukesh on 08/12/2016.
 */

public class CountryCodesDialogFragment extends BaseDialogFragment {

    @BindView(R.id.searchView) SearchView searchView;

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    public static final int REQUEST_CODE_COUNTRY_CODES_DIALOG_FRAGMENT = 432;
    private CountriesListAdapter mCountriesListAdapter;

    @Override
    public boolean getIsFullScreenDialog() {
        return true;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialogfragment_country_code;
    }

    @Override
    public void init() {
        // set title
        tvTitle.setText(getString(R.string.select_country_code));

        // set recyclerview
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration
                .Builder(getActivity())
                .colorResId(R.color.colorDivider)
                .sizeResId(R.dimen.divider_height).showLastDivider()
                .margin(0, 0).build());
        mCountriesListAdapter = new CountriesListAdapter(this,
                getCountriesList());
        recyclerView.setAdapter(mCountriesListAdapter);

        // handle search
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mCountriesListAdapter.searchCountry(newText.trim().toLowerCase());
                return false;
            }
        });
    }

    private List<Country> getCountriesList() {
        List<Country> countriesList = new ArrayList<>();
        InputStream inputStream = getResources().openRawResource(R.raw.countries_data);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            inputStream.close();
            String jsonString = writer.toString();
            if (null != jsonString && !jsonString.isEmpty()) {
                JSONArray jsonArray = new JSONArray(jsonString);
                Gson gson = new Gson();
                for (int i = 0; i < jsonArray.length(); i++) {
                    countriesList.add(gson.fromJson(jsonArray.getString(i), Country.class));
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return countriesList;
    }
}
