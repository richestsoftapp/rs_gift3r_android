package com.richestsoft.gift3r.views.activities;

import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceId;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.presenters.MainPresenter;
import com.richestsoft.gift3r.utils.ApplicationGlobal;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.MainView;
import com.richestsoft.gift3r.views.fragments.SignInFragment;

public class MainActivity extends BaseAppCompatActivity implements MainView {

    private MainPresenter mMainPresenter;

    @Override public boolean isMakeStatusBarTransparent() {
        return true;
    }

    @Override public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override public void init() {
        //initialize and attach presenter
     //   FirebaseApp.initializeApp(this);

       // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        ApplicationGlobal.setFirebasetoken(refreshedToken);
//        Log.e("refreshed token",refreshedToken);

        mMainPresenter = new MainPresenter();
        mMainPresenter.attachView(this);

        // check if the user is already logged in
        mMainPresenter.checkForLoginedStatus();


    }

    @Override public BasePresenter getPresenter() {
        return null;
    }

    //presenter callbacks (MainView)
    @Override public void navigateToHomeScreen() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    //presenter callbacks (MainView)
    @Override public void navigateToSignInScreen() {
        GeneralFunctions.addFragment(getFragmentManager(), 0, 0, 0, 0, new SignInFragment(), null, R.id.clFragContainerMain);
    }

    @Override
    public void onBackPressed() {
        if (null != getFragmentManager() && 1 < getFragmentManager().getBackStackEntryCount()) {
            getFragmentManager().popBackStackImmediate();
        } else {
            finish();
            super.onBackPressed();
        }
    }

}
