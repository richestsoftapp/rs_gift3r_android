package com.richestsoft.gift3r.views.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.pojos.Tab;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.views.adapters.TabsAdapter;
import com.richestsoft.gift3r.views.fragments.MyCardsFragment;
import com.richestsoft.gift3r.views.fragments.MyContacts;
import com.richestsoft.gift3r.views.fragments.MyRegistry;
import com.richestsoft.gift3r.views.fragments.SettingsFragment;
import com.richestsoft.gift3r.views.fragments.StoreListFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Mukesh on 13/02/2017.
 */

public class HomeActivity extends BaseAppCompatActivity {

    @BindView(R.id.tabLayout) TabLayout tabLayout;

    @BindView(R.id.viewPager) ViewPager viewPager;

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.tvTitle) TextView tvTitle;

    @BindView(R.id.ivRightIcon) ImageView ivRightIcon;

    private List<Tab> tabsList = new ArrayList<>();
    public static ViewPager viewPager1;

    @Override
    public boolean isMakeStatusBarTransparent() {
        return false;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    public void init() {

        checkpermissions(HomeActivity.this);
      /*  String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("refreshed token is",refreshedToken);*/

        // set click listener for viewpager
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                hideSoftKeyboard();
                tvTitle.setText(tabsList.get(position).getTabName());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public BasePresenter getPresenter() {
        return null;
    }


    public  void checkpermissions(Activity activity)
    {
        if (Build.VERSION.SDK_INT>=23) {
            new TedPermission(activity)
                    .setPermissionListener(permissionlistener)
                    //.setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.INTERNET,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE,Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS)
                    .check();
        }
    }

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            //Toast.makeText(Drawer_Activity.this, "Permission Granted", Toast.LENGTH_SHORT).show();


            tabsList.add(new Tab(new StoreListFragment(), getString(R.string.title_fragment_stores_list), R.drawable.drawable_store_tab_icon_selector, false));
            tabsList.add(new Tab(new MyCardsFragment(), getString(R.string.title_fragment_my_gifts), R.drawable.drawable_my_gift_cards_tab_icon_selector, false));
            tabsList.add(new Tab(new MyContacts(), getString(R.string.title_fragment_my_contacts), R.drawable.drawable_my_contacts_cards_tab_icon_selector, false));
            tabsList.add(new Tab(new MyRegistry(), getString(R.string.title_fragment_my_registry), R.drawable.drawable_my_favourites_cards_tab_icon_selector, false));
            tabsList.add(new Tab(new SettingsFragment(), getString(R.string.title_fragment_settings), R.drawable.drawable_settings_tab_icon_selector, false));

            // setup tab adapter
            viewPager.setOffscreenPageLimit(5);
            viewPager.setAdapter(new TabsAdapter(getFragmentManager(), tabsList));
            tabLayout.setupWithViewPager(viewPager);

            // set icons for tabs
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                tabLayout.getTabAt(i).setIcon(tabsList.get(i).getTabIcon());
            }

            tvTitle.setText(tabsList.get(0).getTabName());
            viewPager1=viewPager;

            try {

                if (getIntent().getStringExtra("type").equalsIgnoreCase("buycards")){

                    viewPager.setCurrentItem(1);
                    tvTitle.setText(tabsList.get(1).getTabName());

                }else if (getIntent().getStringExtra("type").equalsIgnoreCase("stores")){
                    viewPager.setCurrentItem(0);
                    tvTitle.setText(tabsList.get(0).getTabName());

                }else if (getIntent().getStringExtra("type").equalsIgnoreCase("registry")){
                    viewPager.setCurrentItem(3);
                    tvTitle.setText(tabsList.get(3).getTabName());
                }else if (getIntent().getStringExtra("type").equalsIgnoreCase("contact")){
                    viewPager.setCurrentItem(2);
                    tvTitle.setText(tabsList.get(2).getTabName());
                }

            }catch (Exception e){
                e.printStackTrace();
            }



            //viewPager.getAdapter().notifyDataSetChanged();
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            // initialize list of tabs



        }
    };
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
