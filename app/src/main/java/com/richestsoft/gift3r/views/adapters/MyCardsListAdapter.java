package com.richestsoft.gift3r.views.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.drawee.view.SimpleDraweeView;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.pojos.GiftCard;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.fragments.RedeemCardFragment;
import com.richestsoft.gift3r.views.interfaces.SendGiftCardsCallback;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by user28 on 9/2/18.
 */

public class MyCardsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private LayoutInflater mLayoutInflater;
    private List<GiftCard> cardList;
    private Fragment mFragment;
    public static Double price;
    private SendGiftCardsCallback mSendGiftCardsCallback;

    public MyCardsListAdapter(Fragment fragment) {
        mFragment = fragment;
        mSendGiftCardsCallback = (SendGiftCardsCallback) fragment;
        mLayoutInflater = LayoutInflater.from(fragment.getActivity());
        cardList = new ArrayList<>();
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyCardViewHolder(mLayoutInflater.inflate(R.layout.row_my_cards, parent, false));
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyCardViewHolder myCardViewHolder = (MyCardViewHolder) holder;
        GiftCard giftCard = cardList.get(position);


        if (cardList.get(position).getMessage()==null) {
            myCardViewHolder.ivshowmessage.setImageResource(R.drawable.messageicon);
        }else if (cardList.get(position).getMessage().equalsIgnoreCase("")){
            myCardViewHolder.ivshowmessage.setImageResource(R.drawable.messageicon);
        }else {
            myCardViewHolder.ivshowmessage.setImageResource(R.drawable.messageicon_red);
        }

        myCardViewHolder.sdvStoreImage.setImageURI(giftCard.getStore_image());
        myCardViewHolder.tvCardName.setText(giftCard.getGift_card_name());
        myCardViewHolder.tvStoreName.setText(giftCard.getStore_name());
        myCardViewHolder.tvAvailableBal.setText(String.format(mFragment.getString(R.string.available_balance_text_format), giftCard.getAvailable_balance()));
    }

    public void updateList(ArrayList<GiftCard> cardList) {
        // add data to main list
        this.cardList.clear();
        this.cardList.addAll(cardList);
        notifyDataSetChanged();
    }

    @Override public int getItemCount() {
        return cardList.size();
    }

    class MyCardViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cardViewMyCard) CardView cardViewMyCard;
        @BindView(R.id.sdvStoreImage) SimpleDraweeView sdvStoreImage;
        @BindView(R.id.tvCardName) TextView tvCardName;
        @BindView(R.id.tvAvailableBal) TextView tvAvailableBal;
        @BindView(R.id.tvStoreName) TextView tvStoreName;
        @BindView(R.id.ivshowmessage) ImageView ivshowmessage;


        MyCardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            // set cardView width
//            tvRedeem.getLayoutParams().width = cardViewMyCard.getLayoutParams().height;
//            tvRedeem.getLayoutParams().height = cardViewMyCard.getLayoutParams().height;

        }

        @OnClick({R.id.btnRedeem, R.id.btnSend, R.id.ivCardInfo,R.id.ivshowmessage})
        public void onClicked(View view) {
            switch (view.getId()) {
                case R.id.btnRedeem:

                    price=cardList.get(getAdapterPosition()).getAvailable_balance();
               //     Log.e("redeem",String.valueOf(cardList.get(getAdapterPosition()).getPrice()));
                    GeneralFunctions.addFragment(mFragment.getFragmentManager(), R.animator.slide_right_in, 0, 0, R.animator.slide_right_out, RedeemCardFragment.newInstance(cardList.get(getAdapterPosition()).getGift_card_id(),cardList.get(getAdapterPosition()).getMypurchasedcard_id(),price), null, R.id.flFragmentContainerHome);
                    break;

                case R.id.btnSend:
                    WriteNotePopUp(1);
                    break;

                case R.id.ivshowmessage:

                    if (cardList.get(getAdapterPosition()).getMessage()==null) {
                     //   ReadNototes("No Message Found !");
                        Toast.makeText(mFragment.getActivity(), "No Message Found !", Toast.LENGTH_SHORT).show();

                    }else if (cardList.get(getAdapterPosition()).getMessage().equalsIgnoreCase("")){
                    //    ReadNototes("No Message Found !");
                        Toast.makeText(mFragment.getActivity(), "No Message Found !", Toast.LENGTH_SHORT).show();
                    }else {
                        ReadNototes(cardList.get(getAdapterPosition()).getMessage());
                    }

                    break;

                case R.id.ivCardInfo:

                    AlertDialog.Builder builder = new AlertDialog.Builder(mFragment.getActivity());
                    builder.setMessage(R.string.cards_disclaimer)
                            .setCancelable(true)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                    break;
            }
        }

        private void WriteNotePopUp(int type) {

            final Dialog dialog = new Dialog(mFragment.getActivity());
            dialog.setContentView(R.layout.writenotepopup);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;

            dialog.getWindow().setAttributes(lp);
            // set the custom dialog components - text, image and button
            Button btn_sendnote = (Button) dialog.findViewById(R.id.btn_sendnote);
            final EditText et_writenotes = (EditText) dialog.findViewById(R.id.et_writenotes);
            final TextView tv_readnote = (TextView) dialog.findViewById(R.id.tv_readnote);
            final RelativeLayout rl_writenotes = (RelativeLayout) dialog.findViewById(R.id.rl_writenotes);

            if (type==0){
                btn_sendnote.setVisibility(View.GONE);
                et_writenotes.setEnabled(false);
                et_writenotes.setFocusable(false);
                et_writenotes.setText(cardList.get(getAdapterPosition()).getMessage());
                tv_readnote.setText("READ NOTE");
                rl_writenotes.setBackground(null);
            }else {
                btn_sendnote.setVisibility(View.VISIBLE);
                et_writenotes.setEnabled(true);
                et_writenotes.setFocusable(true);
                tv_readnote.setText("WRITE A NOTE");
            }


            btn_sendnote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSendGiftCardsCallback.sendGiftCard(cardList.get(getAdapterPosition()).getGift_card_id(),cardList.get(getAdapterPosition()).getMypurchasedcard_id(),et_writenotes.getText().toString().trim());

                    dialog.dismiss();
                }
            });

            dialog.show();

        }
    }

    private void ReadNototes(String msg) {


        final Dialog dialog = new Dialog(mFragment.getActivity());
        dialog.setContentView(R.layout.readnotespopup);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        // set the custom dialog components - text, image and button
        Button btn_sendnote = (Button) dialog.findViewById(R.id.btn_sendnote);
        final TextView et_readnotes = (TextView) dialog.findViewById(R.id.et_readnotes);
        final TextView tv_readnote = (TextView) dialog.findViewById(R.id.tv_readnote);
        final RelativeLayout rl_writenotes = (RelativeLayout) dialog.findViewById(R.id.rl_writenotes);
        et_readnotes.setText(msg);
//        tv_readnote.setText("READ NOTE");
     /*   if (type==0){
            btn_sendnote.setVisibility(View.GONE);
            et_writenotes.setEnabled(false);
            et_writenotes.setFocusable(false);
            et_writenotes.setText(cardList.get(getAdapterPosition()).getMessage());

            rl_writenotes.setBackground(null);
        }else {
            btn_sendnote.setVisibility(View.VISIBLE);
            et_writenotes.setEnabled(true);
            et_writenotes.setFocusable(true);
            tv_readnote.setText("WRITE A NOTE");
        }


        btn_sendnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSendGiftCardsCallback.sendGiftCard(cardList.get(getAdapterPosition()).getGift_card_id(),cardList.get(getAdapterPosition()).getMypurchasedcard_id(),et_writenotes.getText().toString().trim());

                dialog.dismiss();
            }
        });*/

        dialog.show();

    }
}
