package com.richestsoft.gift3r.views.interfaces;

/**
 * Created by user28 on 7/3/18.
 */

public interface BuyGiftCardCallBacks extends SendGiftCardsCallback {

    void buyGiftCard(Integer cardId, String phoneNumber,String message);

}
