package com.richestsoft.gift3r.views;

/**
 * Created by user28 on 13/2/18.
 */

public interface SignInView extends BaseView {

    void navigateToHomeScreen();

}
