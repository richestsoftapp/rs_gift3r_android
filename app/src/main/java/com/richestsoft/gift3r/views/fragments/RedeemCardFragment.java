package com.richestsoft.gift3r.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.presenters.RedeemCardPresenter;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.RedeemCardView;
import java.text.NumberFormat;
import java.util.Locale;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by user28 on 12/2/18.
 */

public class RedeemCardFragment extends BaseFragment implements RedeemCardView, TextWatcher {

    private String current = "";
    public static String str_amount;
    @BindView(R.id.etAmount) EditText etAmount;
    Double price;
  /*  @BindView(R.id.tv_doller) TextView tv_doller;*/
    @BindView(R.id.etReceiptNo) EditText etReceiptNo;
    private RedeemCardPresenter mRedeemCardPresenter;

    public static RedeemCardFragment newInstance(Integer cardId,String purchasecardid,Double price) {
        RedeemCardFragment redeemCardFragment = new RedeemCardFragment();
        Bundle bundle = new Bundle();

        bundle.putInt(RedeemCardConfirmationFragment.BUNDLE_EXTRAS_CARD_ID, cardId);
        bundle.putString("purchasecardid", purchasecardid);
    //    bundle.putDouble("price", price);
        bundle.putDouble("price", price);
        redeemCardFragment.setArguments(bundle);
        return redeemCardFragment;
    }

    @Override public int getLayoutId() {
        return R.layout.fargment_redeem_card;
    }

    @Override public void init() {
        //set toolbar title
        tvTitle.setText(R.string.fragment_title_redeem_card);

     //   price=getArguments().getDouble("price");

        //initialize and attach presenter
        mRedeemCardPresenter = new RedeemCardPresenter();
        mRedeemCardPresenter.attachView(this);

        if (null != toolbar) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    getFragmentManager().popBackStackImmediate();

                }
            });
        }

        etAmount.setText("$0.00");
        etAmount.addTextChangedListener(this);

    }

    @Override public BasePresenter getPresenter() {
        return null;
    }

    @OnClick({R.id.btnNext})
    public void onClicked(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
              //  Log.e("price is",String.valueOf(MyCardsListAdapter.price));
                if (etAmount.getText().toString().equals("$0.00")){
                    Toast.makeText(getActivity(), "Please enter total bill amount", Toast.LENGTH_SHORT).show();
                }else {
                    if (null != getArguments()) {

                        str_amount=etAmount.getText().toString().trim();
                        str_amount= str_amount.replaceAll("(?<=\\d),(?=\\d)|\\$", "");

                        Double sendamount=getArguments().getDouble("price");

                  /*  if (sendamount>MyCardsListAdapter.price){
                        Toast.makeText(getActivity(), "Please enter valid price", Toast.LENGTH_SHORT).show();
                    }else {*/
                        mRedeemCardPresenter.proceedToPaymentConfirmation(getArguments().getInt(RedeemCardConfirmationFragment.BUNDLE_EXTRAS_CARD_ID),getArguments().getString("purchasecardid"),String.valueOf(sendamount)/*etAmount.getText().toString()*/, etReceiptNo.getText().toString().trim());
                        //    }

                    }
                }
                break;
        }
    }

    @Override public void proceedToConfirmation(Integer cardId,String purchasecardid, String amount, String receiptNo) {

        GeneralFunctions.addFragment(getFragmentManager(), R.animator.slide_right_in, 0, 0, R.animator.slide_right_out, RedeemCardConfirmationFragment.newInstance(cardId, purchasecardid,amount, receiptNo), null, R.id.flFragmentContainerHome);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


      /*  if(!s.toString().startsWith("$")){
            etAmount.setText("$"+etAmount.getText().toString());
            Selection.setSelection(etAmount.getText(), etAmount.getText().length());

        }
       // Log.e("s is",etAmount.getText().toString());*/


        if(!s.toString().equals(current)){
       etAmount.removeTextChangedListener(this);

            String cleanString = s.toString().replaceAll("[$,.]", "");

            double parsed = Double.parseDouble(cleanString);
            String formatted = NumberFormat.getCurrencyInstance(Locale.US).format((parsed/100));

            current = formatted;
            etAmount.setText(formatted);
            etAmount.setSelection(formatted.length());
            etAmount.addTextChangedListener(this);
        }

    }

    @Override
    public void afterTextChanged(Editable s) {


    }
}
