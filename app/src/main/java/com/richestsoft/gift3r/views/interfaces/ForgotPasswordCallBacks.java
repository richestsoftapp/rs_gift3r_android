package com.richestsoft.gift3r.views.interfaces;

/**
 * Created by user28 on 6/2/18.
 */

public interface ForgotPasswordCallBacks {

    void onForgotPasswordSubmit(String email);

}
