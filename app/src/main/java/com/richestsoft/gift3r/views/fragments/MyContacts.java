package com.richestsoft.gift3r.views.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.models.pojos.Contact_Status_Request;
import com.richestsoft.gift3r.models.pojos.Contact_Status_Response;
import com.richestsoft.gift3r.models.pojos.DataFromDbpojo;
import com.richestsoft.gift3r.models.pojos.Userdetail;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.views.adapters.All_Contacts_Adapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class MyContacts extends BaseFragment implements SearchView.OnQueryTextListener {

    View view;
    int currentItems, totalItems, scrollOutItems;
    public static int CURRENTPAGE=1;
    RecyclerView rv_all_contacts;
    SearchView search;
    List<DataFromDbpojo> alldatalist = new ArrayList();
    List<Userdetail> userdetailArrayList = new ArrayList<>();
    List<Userdetail> list_pagination = new ArrayList<>();
    List<Userdetail> sendinglist = new ArrayList<>();
    List<String> namelist = new ArrayList<>();
    List<String> imagelist = new ArrayList<>();
    List<String> numberlist = new ArrayList<>();
    List<String> statuslist = new ArrayList<>();
    LinearLayoutManager manager;
    List<Userdetail> usercontactdetail;
    List<Userdetail> pag_usercontactdetail;
    public int e_point=1;
    public int s_point=0,totallistsize=0;
    //   List<Integer> statuslist = new ArrayList<>();
    Boolean isScrolling = false;
    SwipeRefreshLayout srl_contacts;
    ProgressBar pb_allchatsbottom;
    All_Contacts_Adapter all_contacts_adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_my_contacts, container, false);
        Init();
        return view;
    }

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public void init() {

    }

    @Override
    public BasePresenter getPresenter() {
        return null;
    }

    @SuppressLint("LongLogTag")
    private void Init() {

        rv_all_contacts=view.findViewById(R.id.rv_all_contacts);
        pb_allchatsbottom=view.findViewById(R.id.pb_allchatsbottom);
        srl_contacts=view.findViewById(R.id.srl_contacts);
        search=view.findViewById(R.id.search);


        manager=new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        rv_all_contacts.setLayoutManager(manager);

        search.setOnQueryTextListener(this);


        srl_contacts.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                search.setQuery("", true);

              LoadAll_Contacts();

            }
        });

        LoadAll_Contacts();
    }

    public void LoadAll_Contacts(){


      //
      //  helper.DeleteData();
        userdetailArrayList.clear();
     //   pag_usercontactdetail.clear();
        list_pagination.clear();
        sendinglist.clear();
        namelist.clear();
        imagelist.clear();
        numberlist.clear();
        e_point=1;
        s_point=0;
        totallistsize=0;
        getContactsDetails();

    }

    private void getContactsDetails() {
        srl_contacts.setRefreshing(true);

        Cursor phones = getActivity().getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                null, null, "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME + ") ASC");

        while (phones.moveToNext()) {
            String Name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String Id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
         //   String total_count = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NAME_RAW_CONTACT_ID));
            String Number = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            String image_uri = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));


            Log.e("name and number",Name+"......"+Number);

            namelist.add(Name);
            numberlist.add(Number);

            if (image_uri != null) {
                imagelist.add(image_uri);

            }else {

                imagelist.add("");
            }


        }

        Get_Contact_Status_API();
     //   AddListData();

    }

    @SuppressLint("LongLogTag")
    private void Get_Contact_Status_API() {

        final Contact_Status_Request contact_status_request=new Contact_Status_Request();
        contact_status_request.setNamelist(namelist);
        contact_status_request.setAddresslist(imagelist);
        contact_status_request.setNumber(numberlist);

        RestClient.get().GetcontactStatus("application/json", contact_status_request)
                .enqueue(new Callback<Contact_Status_Response>() {
                    @Override
                    public void onResponse(Call<Contact_Status_Response> call,
                                           retrofit2.Response<Contact_Status_Response> response) {
                        try {

                            srl_contacts.setRefreshing(false);
                            usercontactdetail=new ArrayList<>();
                            pag_usercontactdetail=new ArrayList<>();

                            Contact_Status_Response contact_status_response=new Contact_Status_Response();
                            contact_status_response=response.body();

                            String temp_name="";
                            String temp_number="";

                            for (int i=0;i<contact_status_response.getNamelist().size();i++){

                           //     Log.e("name and number",contact_status_response.getNamelist().get(i)+"......."+contact_status_response.getNumber().get(i));

                                if (contact_status_response.getNamelist().get(i).equals(temp_name) && contact_status_response.getNumber().get(i).equals(temp_number)){

                                }else {
                                    temp_name=contact_status_response.getNamelist().get(i);
                                    temp_number=contact_status_response.getNumber().get(i);
                                    Userdetail userdetail=new Userdetail();
                                    userdetail.setC_name(response.body().getNamelist().get(i));
                                    userdetail.setC_number(response.body().getNumber().get(i));
                                    userdetail.setStatus(response.body().getStatus().get(i));

                                    if (response.body().getAddresslist().get(i)==null){
                                        userdetail.setC_photo("");
                                    }else {

                                        userdetail.setC_photo(response.body().getAddresslist().get(i));
                                    }

                                    usercontactdetail.add(userdetail);

                                }



                        /*  if (alldatalist.size()>0){
                             // Log.e("database check ","greater than 0");
                          }else {
                          //    Log.e("database check ","less than 0");
                              helper.insertData(response.body().getNamelist().get(i),response.body().getNumber().get(i),response.body().getAddresslist().get(i),String.valueOf(response.body().getStatus().get(i)));
                          }*/

                            }

                        //    Log.e("usercontact  detail size",String.valueOf(usercontactdetail.size()));

                        //    Log.e("user contact detail size",String.valueOf(usercontactdetail.size()));
                            totallistsize=usercontactdetail.size();

                           /* for (int i=0;i<usercontactdetail.size();i++){
                                Log.e("userlist name",usercontactdetail.get(i).getC_name());
                                Log.e("userlist number",usercontactdetail.get(i).getC_number());
                                Log.e("userlist status",String.valueOf(usercontactdetail.get(i).getStatus()));
                                Log.e("userlist image",usercontactdetail.get(i).getC_photo());

                            }*/

                            all_contacts_adapter=new All_Contacts_Adapter(getActivity(),pag_usercontactdetail,usercontactdetail);
                            rv_all_contacts.setAdapter(all_contacts_adapter);
                            all_contacts_adapter.notifyDataSetChanged();

                            Loadmoredata();

                        } catch (Exception e) {
                            //           myCustomLoader.hidedialog();
                            e.printStackTrace();

                            //  myCustomLoader.showToast(mContext.getString(R.string.error_general));
                        }
                    }

                    @Override
                    public void onFailure(Call<Contact_Status_Response> call, Throwable t) {
                   //     Log.e("")
                        //   ivFavourite.setEnabled(true);
                        //    myCustomLoader.hidedialog();
                        //      myCustomLoader.handleRetrofitError(mFragment.getView(), String.valueOf(t));
                    }
                });

    }
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text = newText;
        all_contacts_adapter.filter(text);
      //  all_contacts_adapter.notifyDataSetChanged();
        return false;
    }

    public void Loadmoredata(){

     //   Log.e("start point and ",String.valueOf(s_point)+"......"+String.valueOf(e_point));
        List<Userdetail> pag_usercontactdetailobject=new ArrayList<>();


            for (int i=s_point;i<e_point;i++){
                pag_usercontactdetailobject.add(usercontactdetail.get(i));
            }

            pag_usercontactdetail.addAll(pag_usercontactdetailobject);
            all_contacts_adapter.notifyDataSetChanged();


            s_point=e_point;
            e_point=e_point+1;

            if (e_point<=totallistsize){
                Loadmoredata();
            }

    }
}
