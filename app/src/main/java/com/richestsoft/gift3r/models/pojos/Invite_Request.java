package com.richestsoft.gift3r.models.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Invite_Request {

    @SerializedName("phone_number")
    @Expose
    private String phone_number;

    @SerializedName("session_id")
    @Expose
    private String session_id;

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }
}
