package com.richestsoft.gift3r.models.networkrequest;

/**
 * Created by Mukesh on 17/02/2017.
 */

public class WebConstants {

   // public static final String ACTION_BASE_URL = "http://demo.richestsoft.com/GIFT3R/GIFT3R/public/api/v1/";

 //http://demo.richestsoft.in/GIFT3R/public/api/v1
   // public static final String ACTION_BASE_URL = "http://demo.richestsoft.in/GIFT3R/public/api/v1/";
    public static final String ACTION_BASE_URL = "http://18.221.106.68/gift3r/public/api/v1/";
    public static final String ACTION_ABOUT_US = ACTION_BASE_URL + "about-us";
    public static final String ACTION_TERMS_AND_CONDITIONS = ACTION_BASE_URL + "terms";
    public static final String ACTION_TERMS_OF_USE = ACTION_BASE_URL + "terms-use";
    public static final String ACTION_PRIVACY_POLICY = ACTION_BASE_URL + "policy";

    // Date Time Format Constants
    public static final String DATE_FORMAT_SERVER = "yyyy-MM-d";
    public static final String DATE_FORMAT_DISPLAY = "d MMM, yyyy";
}