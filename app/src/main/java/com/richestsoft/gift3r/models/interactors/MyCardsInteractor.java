package com.richestsoft.gift3r.models.interactors;

import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.utils.ApplicationGlobal;

import io.reactivex.disposables.Disposable;

/**
 * Created by user28 on 9/2/18.
 */

public class MyCardsInteractor extends BaseInteractor {

    public Disposable getMyCardsLists(String searchKey, NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().getMyCards(ApplicationGlobal.getSessionId(),
                searchKey, ApplicationGlobal.getDeviceLocale()),
                networkRequestCallbacks);
    }

    public Disposable sendMyCard(Integer cardId,String purchasedcard_id, String phoneNumber,String message, NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().sendCard(ApplicationGlobal.getSessionId(),
                cardId, purchasedcard_id,phoneNumber, ApplicationGlobal.getDeviceLocale(),message),
                networkRequestCallbacks);
    }

}
