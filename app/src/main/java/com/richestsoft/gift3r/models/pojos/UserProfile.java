package com.richestsoft.gift3r.models.pojos;

/**
 * Created by Mukesh on 06/06/2016.
 */
public class UserProfile {

    private Integer user_id;
    private String name;
    private String email;
    private String image;
    private String phone_number;

    public Integer getUser_id() {
        return user_id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getImage() {
        return image;
    }

    public String getPhone_number() {
        return phone_number;
    }
}
