package com.richestsoft.gift3r.models.interactors;

import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.utils.ApplicationGlobal;
import com.richestsoft.gift3r.utils.GeneralFunctions;

import io.reactivex.disposables.Disposable;

import static com.richestsoft.gift3r.utils.Constants.DEVICE_TYPE;

/**
 * Created by user28 on 8/2/18.
 */

public class OtpVerificationInteractor extends BaseInteractor {

    public Disposable signUpUser(String email, String countryCode, String phoneNumber,
                                 String name, String password, String otp,
                                 final NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().signUp(email, countryCode, phoneNumber, name,
                password, otp, GeneralFunctions.generateRandomString(15), DEVICE_TYPE,
                ApplicationGlobal.getFirebasetoken(), ApplicationGlobal.getDeviceLocale()),
                networkRequestCallbacks);

    }

}
