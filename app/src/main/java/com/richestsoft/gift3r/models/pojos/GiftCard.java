package com.richestsoft.gift3r.models.pojos;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user28 on 9/2/18.
 */

public class GiftCard implements Parcelable {

    private String store_name = "";
    private Integer gift_card_id;
    private Double available_balance;
    private Double price;
    private String gift_card_name = "";
    private String store_image = "";
    private String mypurchasedcard_id = "";
    private String message = "";

    public GiftCard() {
    }

    public Integer getGift_card_id() {
        return gift_card_id;
    }

    public Double getAvailable_balance() {
        return available_balance;
    }

    public String getGift_card_name() {
        return gift_card_name;
    }

    public String getStore_image() {
        return store_image;
    }

    public String getStore_name() {
        return store_name;
    }

    public String getMessage() {
        return message;
    }

    public String getMypurchasedcard_id() {
        return mypurchasedcard_id;
    }

    public Double getPrice() {
        return price;
    }


    protected GiftCard(Parcel in) {
        store_name = in.readString();
        gift_card_id = in.readByte() == 0x00 ? null : in.readInt();
        available_balance = in.readByte() == 0x00 ? null : in.readDouble();
        price = in.readByte() == 0x00 ? null : in.readDouble();
        gift_card_name = in.readString();
        mypurchasedcard_id = in.readString();
        message = in.readString();
        store_image = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(store_name);
        if (gift_card_id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(gift_card_id);
        }
        if (available_balance == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(available_balance);
        }
        if (price == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(price);
        }
        dest.writeString(gift_card_name);
        dest.writeString(mypurchasedcard_id);
        dest.writeString(message);
        dest.writeString(store_image);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GiftCard> CREATOR = new Parcelable.Creator<GiftCard>() {
        @Override
        public GiftCard createFromParcel(Parcel in) {
            return new GiftCard(in);
        }

        @Override
        public GiftCard[] newArray(int size) {
            return new GiftCard[size];
        }
    };
}
