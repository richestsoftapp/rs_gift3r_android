package com.richestsoft.gift3r.models.interactors;

import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.utils.ApplicationGlobal;

import io.reactivex.disposables.Disposable;

/**
 * Created by user28 on 9/2/18.
 */

public class StoreDetailInteractor extends BaseInteractor {

    public Disposable buyMyCard(Integer cardId, String phoneNumber, String message,NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().buyCard(ApplicationGlobal.getSessionId(),
                cardId, phoneNumber, ApplicationGlobal.getDeviceLocale(),message),
                networkRequestCallbacks);
    }

   /* public Disposable AddFavorite(String store_id, NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().AddFavorite(ApplicationGlobal.getSessionId(),store_id),
                networkRequestCallbacks);
    }*/

}
