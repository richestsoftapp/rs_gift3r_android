package com.richestsoft.gift3r.models.interactors;

import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.utils.ApplicationGlobal;

import io.reactivex.disposables.Disposable;

/**
 * Created by user28 on 13/2/18.
 */

public class RedeemCardConfirmationInteractor extends BaseInteractor {

    public Disposable redeemCard(Integer cardId, String purchasecardid,String receiptNumber, String billAmount, String redeemedAmount,
                                 NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().redeemCard(ApplicationGlobal.getSessionId(), cardId, purchasecardid, receiptNumber, billAmount, redeemedAmount, ApplicationGlobal.getDeviceLocale()), networkRequestCallbacks);
    }

}
