package com.richestsoft.gift3r.models.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Get_User_Detail {
    @SerializedName("profile")
    @Expose
    private Profile profile;
    @SerializedName("favouritelist")
    @Expose
    private List<Store> favouritelist = null;

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<Store> getFavouritelist() {
        return favouritelist;
    }

    public void setFavouritelist(List<Store> favouritelist) {
        this.favouritelist = favouritelist;
    }
}
