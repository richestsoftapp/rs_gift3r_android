package com.richestsoft.gift3r.models.interactors;

import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.utils.ApplicationGlobal;

import io.reactivex.disposables.Disposable;

/**
 * Created by user28 on 8/2/18.
 */

public class StoreListInteractor extends BaseInteractor {

    public Disposable getStoreList(String searchKey, Integer pageLimit, Integer page, NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().getStoreList(ApplicationGlobal.getSessionId(),
                searchKey, pageLimit, page, ApplicationGlobal.getDeviceLocale()), networkRequestCallbacks);
    }
}
