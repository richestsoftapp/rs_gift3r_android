package com.richestsoft.gift3r.models.pojos;

/**
 * Created by Mukesh on 26/05/2016.
 */
public class PojoRegister {

    private String fullName = "";
    private String email = "";
    private String facebookId = "";
    private String googlePlusId = "";
    private String profileImage = "";

    public PojoRegister() {
    }

    public PojoRegister(String fullName, String email, String facebookId, String googlePlusId,
                        String profileImage) {
        this.fullName = fullName;
        this.email = email;
        this.facebookId = facebookId;
        this.googlePlusId = googlePlusId;
        this.profileImage = profileImage;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public String getGooglePlusId() {
        return googlePlusId;
    }

    public String getProfileImage() {
        return profileImage;
    }

}