package com.richestsoft.gift3r.models.pojos;

/**
 * Created by Mukesh on 31/08/2016.
 */
public class PojoUserSignIn {

    private String message = "";
    private String session_id = "";
    private UserProfile profile;

    public String getMessage() {
        return message;
    }

    /**
     * @return The session_id
     */
    public String getSession_id() {
        return session_id;
    }

    /**
     * @return The profile
     */
    public UserProfile getProfile() {
        return profile;
    }

    /**
     * @param profile The profile
     */
    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }

}
