package com.richestsoft.gift3r.models.interactors;

import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.utils.ApplicationGlobal;

import io.reactivex.disposables.Disposable;

/**
 * Created by user28 on 22/2/18.
 */

public class SignUpInteractor extends BaseInteractor {

    public Disposable getOtp(String countryCode, String phoneNumber, String email,
                             NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().getOtp(countryCode, phoneNumber, email,
                ApplicationGlobal.getDeviceLocale()), networkRequestCallbacks);
    }

}
