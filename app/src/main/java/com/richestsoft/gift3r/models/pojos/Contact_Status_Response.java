package com.richestsoft.gift3r.models.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Contact_Status_Response {
    @SerializedName("number")
    @Expose
    private List<String> number = null;
    @SerializedName("status")
    @Expose
    private List<Integer> status = null;
    @SerializedName("namelist")
    @Expose
    private List<String> namelist = null;
    @SerializedName("addresslist")
    @Expose
    private List<String> addresslist = null;

    public List<String> getNumber() {
        return number;
    }

    public void setNumber(List<String> number) {
        this.number = number;
    }

    public List<Integer> getStatus() {
        return status;
    }

    public void setStatus(List<Integer> status) {
        this.status = status;
    }

    public List<String> getNamelist() {
        return namelist;
    }

    public void setNamelist(List<String> namelist) {
        this.namelist = namelist;
    }

    public List<String> getAddresslist() {
        return addresslist;
    }

    public void setAddresslist(List<String> addresslist) {
        this.addresslist = addresslist;
    }
}
