package com.richestsoft.gift3r.presenters;

import com.richestsoft.gift3r.views.MainView;

/**
 * Created by user28 on 13/2/18.
 */

public class MainPresenter extends BasePresenter<MainView> {

    public void checkForLoginedStatus() {
        if (getView().getUserPrefsManager().getIsLogined()) {
            getView().navigateToHomeScreen();
        } else {
            getView().navigateToSignInScreen();
        }
    }
}
