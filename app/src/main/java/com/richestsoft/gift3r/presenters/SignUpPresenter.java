package com.richestsoft.gift3r.presenters;

import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.interactors.SignUpInteractor;
import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RetrofitRequest;
import com.richestsoft.gift3r.models.networkrequest.WebConstants;
import com.richestsoft.gift3r.models.pojos.PojoNetworkResponse;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.SignUpView;
import com.richestsoft.gift3r.views.dialogfragments.WebViewDialogFragment;

import butterknife.BindView;
import retrofit2.Response;

/**
 * Created by user28 on 22/2/18.
 */

public class SignUpPresenter extends BasePresenter<SignUpView> {

    private SignUpInteractor mSignUpInteractor;

    @Override public void attachView(SignUpView view) {
        super.attachView(view);
        mSignUpInteractor = new SignUpInteractor();
    }

    public void signUpUser(String email, String countryCode, String phoneNumber,
                           String name, String password) {
        if (name.isEmpty()) {
            getView().showMessage(R.string.enter_name, null, true);
        } else if (TextUtils.isEmpty(email)) {
            getView().showMessage(R.string.enter_emailid_msg, null, true);
        }else if (!GeneralFunctions.isValidEmail(email)) {
            getView().showMessage(R.string.enter_email, null, true);
        } else if (countryCode.isEmpty()) {
            getView().showMessage(R.string.enter_country_code, null, true);
        } else if (phoneNumber.isEmpty()) {
            getView().showMessage(R.string.enter_phone_number, null, true);
        } else  if (password.isEmpty()) {
            getView().showMessage(R.string.enter_password_msg, null, true);
        }
        else if (!GeneralFunctions.isValidPassword(password)) {
            getView().showMessage(R.string.invalid_password, null, true);
        } else {
            getView().showProgressLoader();
            mCompositeDisposable.add(mSignUpInteractor.getOtp("+1", phoneNumber, email,
                    new NetworkRequestCallbacks() {
                        @Override
                        public void onSuccess(Response<?> response) {
                            if (null != getView() && !getView().isFragmentDestroyed()) {
                                getView().hideProgressLoader();
                                try {
                                    PojoNetworkResponse pojoNetworkResponse = RetrofitRequest
                                            .checkForResponseCode(response.code());
                                    if (pojoNetworkResponse.isSuccess() && null != response.body()) {

                                    /*    WebViewDialogFragment.newInstance("signup",
                                                WebConstants.).show(getFragmentManager(), getString(R.string.dialog));
                                        */

                                        getView().navigateToVerification();

                                    } else if (pojoNetworkResponse.isSessionExpired()) {
                                        getView().expireUserSession();
                                    } else {
                                        getView().showMessage(0, RetrofitRequest
                                                        .getErrorMessage(response.errorBody()),
                                                true);
                                    }
                                } catch (Exception e) {
                                    getView().hideProgressLoader();
                                    e.printStackTrace();
                                    getView().showMessage(R.string.retrofit_failure,
                                            null, true);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            if (null != getView() && !getView().isFragmentDestroyed()) {
                                getView().hideProgressLoader();
                                getView().showMessage(RetrofitRequest.getRetrofitError(t),
                                        null, true);
                            }
                        }
                    }));
        }
    }

    @Override public void detachView() {
        mSignUpInteractor = null;
        super.detachView();
    }
}
