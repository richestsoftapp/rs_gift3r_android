package com.richestsoft.gift3r.presenters;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.interactors.SettingsInteractor;
import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RetrofitRequest;
import com.richestsoft.gift3r.models.pojos.PojoCommon;
import com.richestsoft.gift3r.models.pojos.PojoNetworkResponse;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.SettingsView;

import retrofit2.Response;

/**
 * Created by shray on 09-02-2018.
 */

public class SettingsPresenter extends BasePresenter<SettingsView> {

    private SettingsInteractor mSettingsInteractor;

    @Override public void attachView(SettingsView view) {
        super.attachView(view);
        mSettingsInteractor = new SettingsInteractor();
    }

    public void changePassword(String oldPassword, String newPassword) {
        if (!GeneralFunctions.isValidPassword(oldPassword) ||
                !GeneralFunctions.isValidPassword(newPassword)) {
            getView().showMessage(R.string.invalid_password,
                    null, true);
        } else {
            getView().showProgressLoader();
            mCompositeDisposable.add(mSettingsInteractor.changePassword(oldPassword, newPassword,
                    new NetworkRequestCallbacks() {
                        @Override
                        public void onSuccess(Response<?> response) {
                            if (null != getView() && !getView().isFragmentDestroyed()) {
                                getView().hideProgressLoader();
                                try {
                                    PojoNetworkResponse pojoNetworkResponse = RetrofitRequest
                                            .checkForResponseCode(response.code());
                                    if (pojoNetworkResponse.isSuccess() && null != response.body()) {
                                        getView().showMessage(0,
                                                ((PojoCommon) response.body()).getMessage(),
                                                false);

                                        getView().dismissDialogFragment();

                                    } else if (pojoNetworkResponse.isSessionExpired()) {
                                        getView().expireUserSession();
                                    } else {
                                        getView().showMessage(0, RetrofitRequest
                                                        .getErrorMessage(response.errorBody()),
                                                true);
                                    }
                                } catch (Exception e) {
                                    getView().hideProgressLoader();
                                    e.printStackTrace();
                                    getView().showMessage(R.string.retrofit_failure,
                                            null, true);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            if (null != getView() && !getView().isFragmentDestroyed()) {
                                getView().hideProgressLoader();
                                getView().showMessage(RetrofitRequest.getRetrofitError(t),
                                        null, true);
                            }
                        }
                    }));
        }
    }

    public void logout() {
        getView().showProgressLoader();
        mCompositeDisposable.add(mSettingsInteractor.logout(new NetworkRequestCallbacks() {
            @Override
            public void onSuccess(Response<?> response) {
                if (null != getView() && !getView().isFragmentDestroyed()) {
                    getView().hideProgressLoader();
                    try {
                        PojoNetworkResponse pojoNetworkResponse = RetrofitRequest
                                .checkForResponseCode(response.code());
                        if (pojoNetworkResponse.isSuccess() && null != response.body()) {
                            getView().expireUserSession();

                        } else if (pojoNetworkResponse.isSessionExpired()) {
                            getView().expireUserSession();
                        } else {
                            getView().showMessage(0, RetrofitRequest
                                            .getErrorMessage(response.errorBody()),
                                    true);
                        }
                    } catch (Exception e) {
                        getView().hideProgressLoader();
                        e.printStackTrace();
                        getView().showMessage(R.string.retrofit_failure,
                                null, true);
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                if (null != getView() && !getView().isFragmentDestroyed()) {
                    getView().hideProgressLoader();
                    getView().showMessage(RetrofitRequest.getRetrofitError(t),
                            null, true);
                }
            }
        }));
    }

    public void setUserProfile() {
        getView().setUserProfile(getView().getUserPrefsManager().getUserProfile());
    }

    @Override public void detachView() {
        mSettingsInteractor = null;
        super.detachView();
    }
}
