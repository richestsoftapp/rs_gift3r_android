package com.richestsoft.gift3r.presenters;

import android.widget.Toast;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.interactors.RedeemCardConfirmationInteractor;
import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RetrofitRequest;
import com.richestsoft.gift3r.models.pojos.PojoNetworkResponse;
import com.richestsoft.gift3r.views.RedeemCardConfirmationView;

import retrofit2.Response;

/**
 * Created by user28 on 13/2/18.
 */

public class RedeemCardConfirmationPresenter extends BasePresenter<RedeemCardConfirmationView> {

    private RedeemCardConfirmationInteractor mRedeemCardConfirmationInteractor;

    @Override public void attachView(RedeemCardConfirmationView view) {
        super.attachView(view);
        mRedeemCardConfirmationInteractor = new RedeemCardConfirmationInteractor();
    }

    public void redeemCard(Integer cardId, String purchasecardid, final String receiptNumber, String billAmount, String redeemedAmount) {
        getView().showProgressLoader();
        mCompositeDisposable.add(mRedeemCardConfirmationInteractor.redeemCard(cardId, purchasecardid,receiptNumber, billAmount, redeemedAmount, new NetworkRequestCallbacks() {
            @Override
            public void onSuccess(Response<?> response) {
                if (null != getView() && !getView().isFragmentDestroyed()) {
                    getView().hideProgressLoader();
                    try {
                        PojoNetworkResponse pojoNetworkResponse = RetrofitRequest
                                .checkForResponseCode(response.code());
                        if (pojoNetworkResponse.isSuccess() && null != response.body()) {
                            getView().sendBroadcast();

                            Toast.makeText(getView().getActivityContext(),"Amount redeemed to merchant", Toast.LENGTH_SHORT).show();

                        } else if (pojoNetworkResponse.isSessionExpired()) {
                            getView().expireUserSession();
                        } else {
                            getView().showMessage(0, RetrofitRequest
                                            .getErrorMessage(response.errorBody()),
                                    true);
                        }
                    } catch (Exception e) {
                        getView().hideProgressLoader();
                        e.printStackTrace();
                        getView().showMessage(R.string.retrofit_failure,
                                null, true);
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                if (null != getView() && !getView().isFragmentDestroyed()) {
                    getView().hideProgressLoader();
                    getView().showMessage(RetrofitRequest.getRetrofitError(t),
                            null, true);
                }
            }
        }));
    }

    @Override public void detachView() {
        mRedeemCardConfirmationInteractor = null;
        super.detachView();
    }
}
