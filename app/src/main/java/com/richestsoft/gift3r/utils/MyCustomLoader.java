package com.richestsoft.gift3r.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.richestsoft.gift3r.R;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.style.CubeGrid;

/**
 * Created by Mukesh on 06-06-2016.
 */

public class MyCustomLoader {

    private Context mContext;
    //    private ProgressDialog mProgressDialog;
    private Dialog mDialog;

    public MyCustomLoader(Context context) {
        mContext = context;
    }

//    public void showProgressDialog(String message) {
//        if (null == mProgressDialog) {
//            mProgressDialog = new ProgressDialog(mContext);
//        }
//        mProgressDialog.setCancelable(false);
//        mProgressDialog.setMessage(message);
//        mProgressDialog.show();
//    }

//    public void dismissProgressDialog() {
//        if (null != mProgressDialog && mProgressDialog.isShowing()) {
//            mProgressDialog.dismiss();
//        }
//    }

    public void showProgressDialog(String message) {
        mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = ((LayoutInflater) mContext.getSystemService(Context
                .LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_progress_loader, null);
        SpinKitView spinKitView = (SpinKitView) view.findViewById(R.id.spinKitView);
        spinKitView.setIndeterminateDrawable(new CubeGrid());
        mDialog.setContentView(view);
        mDialog.setCancelable(false);
        mDialog.show();
    }

    public void dismissProgressDialog() {
        if (null != mContext && null != mDialog && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    public void showSnackBar(View view, String contentMsg) {
        if (null != mContext && null != view) {
            Snackbar.make(view, contentMsg, Snackbar.LENGTH_LONG)
                    .setAction(mContext.getString(R.string.action_okay),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                }
                            })
                    .setActionTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary))
                    .show();
        }
    }

    public void showToast(String contentMsg) {
        if (null != mContext) {
            Toast.makeText(mContext, contentMsg, Toast.LENGTH_SHORT).show();
        }
    }

    public void showAlertDialog(String title, String message, String positiveButtonText,
                                String negativeButtonText,
                                DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(mContext)
//                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveButtonText, okListener)
                .setNegativeButton(negativeButtonText, null)
                .create()
                .show();
    }

}